import sys
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QFrame, QLabel, QLineEdit, QPushButton, QScrollArea, QVBoxLayout, QHBoxLayout, QWidget, QSplitter
from PyQt5.QtGui import QIcon, QPainter, QPainterPath, QPixmap
from PyQt5.QtCore import QSize, Qt
from integration import db,Active

class Messenger(QWidget):
    def __init__(self,uname):
        self.uname=uname
        print("I am in messenger")
        super().__init__()
        self.init_ui()
        print("UI completed")

    def applyCircularMask(self, pixmap, size, width, height):
        mask = QPixmap(size)
        mask.fill(QtCore.Qt.GlobalColor.transparent)

        painter = QPainter(mask)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)
        path = QPainterPath()
        path.addEllipse(0, 0, width, height)
        painter.setClipPath(path)
        painter.fillPath(path, QtGui.QColor(QtCore.Qt.GlobalColor.white))
        painter.end()
        masked = QPixmap(size)
        masked.fill(QtCore.Qt.GlobalColor.transparent)
        painter.begin(masked)
        painter.setClipPath(path)
        painter.drawPixmap(0, 0, pixmap)
        painter.end()
        return masked

    def init_ui(self):
        doc_ref = db.collection('users').document(self.uname)
        data = doc_ref.get() 
        self.setWindowTitle('Messenger')
        self.setGeometry(20, 40, 1000, 700)
        print("I am in init UI")
        right_layout = QVBoxLayout()
        left_layout = QVBoxLayout()

        #emoji
        emoji_label=QLabel()
        emoji_pixmap=QPixmap("assets/Icons/happy-face.png")
        scaled_emoji_pixmap = emoji_pixmap.scaled(50, 50, Qt.AspectRatioMode.KeepAspectRatio)
        emoji_label.setPixmap(scaled_emoji_pixmap)
        emoji_label.setContentsMargins(5,5,5,5)

        #logo
        logo_label=QLabel()
        logo_pixmap=QPixmap("assets/Logo/comp_logo.png")
        scaled_logo_pixmap = logo_pixmap.scaled(200, 200, Qt.AspectRatioMode.KeepAspectRatio)
        logo_label.setPixmap(scaled_logo_pixmap)
        logo_label.setContentsMargins(5,5,5,5)

        #input box for msg
        msg_edit=QLineEdit("   Enter message")
        msg_edit.setStyleSheet("background-color:white")
        msg_edit.setFixedHeight(30)
        msg_edit.setFixedWidth(300)
     
        #send icon button
        send_btn=QPushButton()
        send_btn.setIcon(QIcon("assets/Icons/send.png"))
        send_btn.setIconSize(QSize(40,40))

        #menu icon button
        menu_btn=QPushButton()
        menu_btn.setIcon(QIcon("assets/Icons/dots.png"))
        menu_btn.setIconSize(QSize(30,30))

        #layout for messages
        msg_disp_layout=QVBoxLayout()
        
        #widget for message display
        msgWid=QWidget()
        msgWid.setLayout(msg_disp_layout)
        msgWid.setFixedHeight(390)
        msgWid.setFixedWidth(625)
        msgWid.setStyleSheet("background-color:#C39BD3;border-radius:6px;")
        scroll_msg_area = QScrollArea()
        scroll_msg_area.setWidget(msgWid)

        # Default lady image
        image_label = QLabel()
        pixmap = QPixmap(data.get('profile_img'))
        if pixmap.isNull():
            print("Image is null")  # Check if the image loading fails
        else:
            circular_pixmap = self.applyCircularMask(pixmap, pixmap.size(), pixmap.width(), pixmap.height())
            scaled_pixmap = circular_pixmap.scaled(120, 120, Qt.AspectRatioMode.KeepAspectRatio)
            image_label.setPixmap(scaled_pixmap)
            image_label.setMargin(2)

    

        # Name of user
        name = QLabel(data.get('fullname'))
        name.setStyleSheet("font-size:30px;color:#FFFFFF")
        name.setContentsMargins(0,0,0,0)
        upper_layout = QHBoxLayout()
        upper_layout.addWidget(image_label)
        upper_layout.addWidget(name)
        upper_layout.addWidget(menu_btn)
        upper_layout.setAlignment(Qt.AlignmentFlag.AlignTop)

        subWidget=QWidget()
        subWidget.setLayout(upper_layout)
        subWidget.setMaximumHeight(150)
        subWidget.setStyleSheet("background-color:#000000")
        
        lower_layout=QVBoxLayout()
 
        #widget for chat
        chatWid=QWidget()
        chatWid.setStyleSheet("background-color:#000000")
        chatWid.setFixedHeight(80)
        chat_layout=QHBoxLayout()
        chat_layout.addWidget(emoji_label)
        chat_layout.addWidget(msg_edit)
        chat_layout.addWidget(send_btn)
        chatWid.setLayout(chat_layout)
        chat_layout.setAlignment(Qt.AlignmentFlag.AlignBottom)

        lower_layout.addWidget(scroll_msg_area)
        lower_layout.addWidget(chatWid)

        subWidget2=QWidget()
        subWidget2.setLayout(lower_layout)
        #subWidget2.setStyleSheet("border:2px solid black")

        left_layout.addWidget(subWidget)
        left_layout.addWidget(subWidget2)
       
        ###############################################################
        
        heading=QLabel("Chat Box")
        heading.setStyleSheet("color:white;font-size:32px;")
        heading.setAlignment(Qt.AlignmentFlag.AlignCenter)
        
        #layout for heading
        heading_layout=QHBoxLayout()
        heading_layout.addWidget(heading)
        heading_layout.addWidget(logo_label)
    

        #widget for heading
        headingWid=QWidget()
        headingWid.setStyleSheet("background-color:black")
        headingWid.setFixedHeight(100)
        headingWid.setLayout(heading_layout)
    
        chat_layout=QVBoxLayout()

        back_button = QPushButton("Back")
        back_button.setStyleSheet("background-color: #7D3C98; color:white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px; box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);")
        back_button.setFixedSize(60,40)
        back_button.clicked.connect(self.exit_page)
        chat_layout.addStretch()
        chat_layout.addWidget(back_button)
        chat_layout.setAlignment(Qt.AlignmentFlag.AlignBottom | Qt.AlignmentFlag.AlignRight)

        chats_sec=QWidget()
        chats_sec.setLayout(chat_layout)
        chats_sec.setStyleSheet("background-color:#C39BD3;border-radius:6px;")
        chats_sec.setFixedWidth(650)
        chats_sec.setFixedHeight(540)


        chats_scroll=QScrollArea()
        chats_scroll.setWidget(chats_sec)


        right_layout.addWidget(headingWid)
        right_layout.addWidget(chats_scroll)

        splitter = QSplitter(Qt.Orientation.Horizontal)
        splitter.addWidget(QWidget())
        splitter.addWidget(QWidget())
        splitter.setSizes([self.width() // 2, self.width() // 2])
        splitter.widget(0).setLayout(left_layout)
        splitter.widget(0).setStyleSheet("border-radius:10px;")
        splitter.widget(1).setStyleSheet("border-radius:10px")
        splitter.widget(1).setLayout(right_layout)

        # Set border radius and border color for both widgets
        widget_style = "border-color:black; border-radius: 10px;"

        splitter.widget(0).setStyleSheet(widget_style)
        splitter.widget(1).setStyleSheet(widget_style)

        main_layout = QVBoxLayout()
        main_layout.addWidget(splitter)
        self.setLayout(main_layout)

    def exit_page(self):
        self.close()



if __name__ =="__main__":
        app = QApplication(sys.argv)
        ex = Messenger('namratachaudhari230')
        ex.setWindowTitle('User Info')
        ex.setGeometry(20, 40, 1000, 700)
        ex.show()
        sys.exit(app.exec_())


