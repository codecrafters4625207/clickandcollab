import sys
from PyQt5.QtCore import QSize, QUrl
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QFormLayout, QHBoxLayout, QLabel, QPushButton, QVBoxLayout, QWidget, QSplitter, QScrollArea,QMessageBox
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtGui import QColor, QPainter, QPainterPath, QPixmap
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QPainter, QPainterPath
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtCore import Qt
from firebase_admin import firestore
from integration import db,Active


from messenger import Messenger
def calculate(total,gain):
    if(total ==None or gain==None or total==0):
        return 0
    else:
        result = (gain * 5) / total
        return "{:.1f}".format(result)
class NewWindow(QWidget):
    def __init__(self, url):
        super().__init__()
        self.url = url
        self.initUI()


    def initUI(self):
        try:
            self.setWindowTitle('New Window')
            self.setGeometry(100, 100, 800, 600)

            layout = QVBoxLayout()

            # Create a QWebEngineView to display the URL content
            web_view = QWebEngineView()
            web_view.load(QUrl(self.url))
            layout.addWidget(web_view)

            self.setLayout(layout)

        except Exception as e:
            print(f"An error occurred in NewWindow: {e}")


class ClickableLabel(QLabel):
    clicked = pyqtSignal()

    def __init__(self, url, parent=None):
        super().__init__(parent)
        self.url = url  # Store the URL for later use

    def mousePressEvent(self, event):
        print("URL:", self.url)  # Print the URL before creating the NewWindow
        new_window = NewWindow(self.url)  # Pass the URL when creating NewWindow
        print("NewWindow instance:", new_window)  # Print the NewWindow instance
        new_window.show()


class LogoButton(QPushButton):
    def __init__(self, url, pixmap_path, parent=None):
        super().__init__(parent)
        self.url = url
        self.setIcon(QIcon(pixmap_path))
        self.setIconSize(QSize(30, 30))
        self.clicked.connect(self.open_url)

    def open_url(self):
        QDesktopServices.openUrl(QUrl(self.url))




class UserInfo(QWidget):
    fav_clicked=False
    startCount=0
    def __init__(self,uname):
        self.rat_label=QLabel("")
        self.uname=uname
        print("I am reached in UserInfo",self.uname)
        super().__init__()
        self.c2w_init_ui()

    def mark_fav(self):
        try:
            if(Active.isLoggedIn):
                UserInfo.fav_clicked = True
                self.fav_label.setIcon(QIcon("assets/Icons/fav_hover.png"))
                
                # Check if the "favorites" collection exists
                favorites_ref = db.collection('favorites').document(Active.activeUser)
                favorites_doc = favorites_ref.get()

                if not favorites_doc.exists:  # If the document doesn't exist, create it
                    favorites_ref.set({self.uname: 1})  # Add sequential number
                else:
                    # Get the current sequential number
                    favorites_data = favorites_doc.to_dict()
                    sequential_no = len(favorites_data) + 1
                    # Update the favorites document with the new sequential number and self.uname
                    favorites_ref.update({self.uname: sequential_no})
                    print("Updated successfully")
            else:
                QMessageBox.critical(self, "Error", "Please Login First!!")
        except Exception as e:
            print(f"An error occurred in mark_fav: {e}")

    def applyCircularMask(self, pixmap, size, width, height):
        mask = QPixmap(size)
        mask.fill(QtCore.Qt.GlobalColor.transparent)

        painter = QPainter(mask)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)
        path = QPainterPath()
        path.addEllipse(0, 0, width, height)
        painter.setClipPath(path)
        painter.fillPath(path, QtGui.QColor(QtCore.Qt.GlobalColor.white))
        painter.end()
        masked = QPixmap(size)
        masked.fill(QtCore.Qt.GlobalColor.transparent)
        painter.begin(masked)
        painter.setClipPath(path)
        painter.drawPixmap(0, 0, pixmap)
        painter.end()
        return masked
    
    def star_clicked(self, index):
        if(Active.isLoggedIn):
            user_ref = db.collection('users').document(self.uname).get()
            self.user_data = user_ref.to_dict()
            doc_ref = db.collection('users').document(self.uname)
            doc_ref.update({
            'no_star': self.user_data.get('no_star')+5,
            'gain_star':self.user_data.get('gain_star')+index+1
            })
            self.rat_label.setText(f"{calculate(self.user_data.get('no_star'),self.user_data.get('gain_star'))}")
            for i in range(index + 1):  # Replace icons up to and including the clicked star
                self.ratings.itemAt(i).widget().setIcon(QIcon("assets/Icons/star.png"))
                

            for i in range(index + 1, 5):  # Reset icons after the clicked star
                self.ratings.itemAt(i).widget().setIcon(QIcon("assets/Icons/star (1).png"))
        else:
            QMessageBox.critical(self, "Error", "Please Login First !!")


    def c2w_init_ui(self):
        try:
            self.user_ref = db.collection('users').document(self.uname).get()
            self.user_data = self.user_ref.to_dict()
            print("Reached to init ui in user info")
            self.fav = "assets/Icons/fav.png"
            # Labels
            header_label = QLabel(self.user_data.get('fullname'))
            header_label.setStyleSheet(
                'color: #FFFFFF;margin:1px;padding: 20px; font-size: 38px; max-height:50px;')
            subheader_label = QLabel(self.user_data.get('profession'))
            subheader_label.setStyleSheet(
                'color: #FFFFFF;margin:1px;padding: 10px; font-size: 24px; max-height:30px;')
            ratings=calculate(self.user_data.get('no_star'),self.user_data.get('gain_star'))
            self.rat_label.setText(f"{ratings}")
            self.rat_label.setStyleSheet('color:white;font-size:17px;')
            
            rat_img_label = QLabel()
            rat_pixmap = QPixmap("assets/Icons/star.png")
            scaled_rat_pixmap = rat_pixmap.scaled(20, 20, Qt.AspectRatioMode.KeepAspectRatio)
            rat_img_label.setPixmap(scaled_rat_pixmap)
            print("I am here 1")
            rat_layout=QHBoxLayout()
            rat_layout.addWidget(self.rat_label)
            rat_layout.addWidget(rat_img_label)
            rat_wid=QWidget()
            rat_wid.setFixedWidth(79)
            rat_wid.setContentsMargins(9,0,0,0)
            rat_wid.setLayout(rat_layout)
            
            self.username_label = QLabel("Username : ")
            self.email_label = QLabel("Email Id : ")
            self.mobno_label = QLabel("Mobile no : ")
            self.profession_label = QLabel("Profession : ")
            self.curr_comp_label = QLabel("Current Company : ")
            self.total_exp_label = QLabel("Total Experience : ")
            self.skills_label = QLabel("Skills : ")
            self.curr_comp_edit = QLabel("None")
            self.ratings_label=QLabel("Give Ratings : ")

            # Details after labels
            self.username_edit = QLabel(self.user_data.get('username'))
            self.email_edit = QLabel(self.user_data.get('email'))
            self.mobno_edit = QLabel(self.user_data.get('mobile'))
            self.profession_edit = QLabel(self.user_data.get('profession'))
            self.skills_edit = QLabel(self.user_data.get('skills'))
            self.total_exp_edit = QLabel(self.user_data.get('experience'))

            self.ratings=QHBoxLayout()
            
            for i in range(5):
                star_button = QPushButton()
                star_button.setIcon(QIcon("assets/Icons/star (1).png"))
                star_button.setIconSize(QSize(22, 22))
                star_button.setFixedHeight(25)
                star_button.setStyleSheet("border: none; background-color: transparent;")
                star_button.clicked.connect(lambda _, index=i: self.star_clicked(index))
                self.ratings.addWidget(star_button)
            self.rat_wid=QWidget()
            self.rat_wid.setFixedWidth(160)
            self.rat_wid.setLayout(self.ratings)
            print("No problem with stars")

            # Styling labels
            self.username_label.setStyleSheet("font-size: 24px")
            self.total_exp_label.setStyleSheet("font-size: 23px")
            self.email_label.setStyleSheet("font-size: 24px")
            self.mobno_label.setStyleSheet("font-size: 24px")
            self.skills_label.setStyleSheet("font-size: 24px")
            self.profession_label.setStyleSheet("font-size: 24px")
            self.curr_comp_label.setStyleSheet("font-size: 24px")
            self.ratings_label.setStyleSheet("font-size:24px;")
            subheader_label.setAlignment(Qt.AlignmentFlag.AlignTop)

            # Styling details after labels
            self.username_edit.setStyleSheet("font-size: 18px")
            self.username_edit.setFixedWidth(300)
            self.email_edit.setStyleSheet("font-size: 18px")
            self.email_edit.setFixedWidth(300)
            self.mobno_edit.setStyleSheet("font-size: 18px")
            self.mobno_edit.setFixedWidth(300)
            self.profession_edit.setStyleSheet("font-size: 18px")
            self.profession_edit.setFixedWidth(500)
            self.skills_edit.setStyleSheet("font-size: 18px")
            self.skills_edit.setFixedWidth(500)
            self.total_exp_edit.setStyleSheet("font-size: 18px")
            self.total_exp_edit.setFixedWidth(500)
            self.curr_comp_edit.setStyleSheet("font-size: 18px")
            self.curr_comp_edit.setFixedWidth(500)

            newWidget = QWidget()
            newWidget.setStyleSheet("background-color: #000000; ")
            newWidget.setFixedWidth(1300)

            # Default lady image
            image_label = QLabel(newWidget)
            img=self.user_data.get('profile_img')
            print(img)
            if(img==None):
                if(self.user_data.get('gender')=='Female'):
                    pixmap = QPixmap("assets/profile/default_lady.jpg")
                else:
                    pixmap=QPixmap("assets/profile/default_men.jpg")
            else:
                pixmap=QPixmap(img)
            circular_pixmap = QPixmap(pixmap)
            if not pixmap.isNull():
                circular_pixmap = self.applyCircularMask(pixmap, pixmap.size(), pixmap.width(), pixmap.height())
            scaled_pixmap = circular_pixmap.scaled(200, 200, Qt.AspectRatioMode.KeepAspectRatio)
            image_label.setPixmap(scaled_pixmap)
            image_label.setMargin(40)
            '''
    fav_label=QLabel(newWidget)
            fav_pixmap=QPixmap(self.fav)
            scaled_fav__pixmap = fav_pixmap.scaled(40, 40, Qt.AspectRatioMode.KeepAspectRatio)
            fav_label.setPixmap(scaled_fav__pixmap)
    '''
            
            self.fav_label=QPushButton()
            self.fav_label.setIcon(QIcon(self.fav))
            self.fav_label.setIconSize(QSize(40,40))
            self.fav_label.clicked.connect(self.mark_fav) 
            print("fav logo added")
            # LinkedIn logo button
            linkedin_button = LogoButton(self.user_data.get('linkedin'), "assets/Logo/linkdin.png")

            # Instagram logo button
            instagram_button = LogoButton(self.user_data.get('instagram'), "assets/Logo/instagram.png")

            # Facebook logo button
            facebook_button = LogoButton(self.user_data.get('facebook'), "assets/Logo/facebook.png")

            # Git logo button
            git_button = LogoButton(self.user_data.get('gitlab'), "assets/Logo/gitlab.png")
            
            # Chat button
            self.chat = QPushButton("Chat Room")
            self.chat.setMaximumWidth(120)
            self.chat.setMaximumHeight(40)
            self.chat.setStyleSheet(
                "margin: 1px 10px 1px 1px;background-color: #7D3C98; color: white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px;")
            self.chat.clicked.connect(self.open_chat_room)
            print("chat room added")
            # Adjusting chat button position
            chat_button_margin = QHBoxLayout()
            chat_button_margin.addWidget(self.chat, Qt.AlignmentFlag.AlignLeft)
            chat_button_margin.addWidget(self.fav_label)
            chat_button_margin.setContentsMargins(40, 0, 120, 0)

            # Back button
            self.back = QPushButton("Back")
            self.back.setMaximumWidth(120)
            self.back.setMaximumHeight(90)
            self.back.clicked.connect(self.exit_page)
            self.back.setStyleSheet(
                "margin: 1px;background-color: #7D3C98;margin:20px;color: white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px; ")

            # Edit profile information
            edit_profile_picture_btn = QPushButton("Mark as favorite")
            edit_profile_info_btn = QPushButton("Contact")
            edit_profile_info_btn.setFixedWidth(220)
            edit_profile_picture_btn.setFixedWidth(220)

            # Creating form and adding elements
            form_layout = QFormLayout()
            form_layout.addRow(self.username_label, self.username_edit)
            form_layout.addRow(self.email_label, self.email_edit)
            form_layout.addRow(self.mobno_label, self.mobno_edit)
            form_layout.addRow(self.profession_label, self.profession_edit)
            form_layout.addRow(self.skills_label, self.skills_edit)
            form_layout.addRow(self.total_exp_label, self.total_exp_edit)
            form_layout.addRow(self.curr_comp_label, self.curr_comp_edit)
            form_layout.addRow(self.ratings_label,self.rat_wid)
            form_layout.addRow(QLabel(""))
            form_layout.setSpacing(20)
            form_layout.setContentsMargins(20, 0, 0, 0)
            form_layout.setFormAlignment(Qt.AlignmentFlag.AlignTop)

            left_layout = QVBoxLayout()
            left_layout.addLayout(form_layout)
            left_layout.addStretch()
            left_layout.addWidget(self.back, alignment=Qt.AlignmentFlag.AlignRight)

            # Setting child layouts for right
            vchild1_layout = QVBoxLayout()
            vchild1_layout.addWidget(image_label)

            header_layout = QVBoxLayout()
            header_layout.addWidget(header_label)
            header_layout.addWidget(subheader_label, alignment=Qt.AlignmentFlag.AlignTop)
            header_layout.addWidget(rat_wid)
            header_layout.addLayout(chat_button_margin)
            header_layout.setSpacing(2)
            header_layout.setContentsMargins(0, 0, 0, 30)

            vchild3_layout = QHBoxLayout()
            vchild3_layout.addWidget(linkedin_button)
            vchild3_layout.addWidget(git_button)
            vchild3_layout.addWidget(instagram_button)
            vchild3_layout.addWidget(facebook_button)
            vchild3_layout.setAlignment(Qt.AlignmentFlag.AlignBottom)
            vchild3_layout.setContentsMargins(2, 0, 5, 10)

            # Setting layouts
            right_layout = QHBoxLayout(newWidget)
            right_layout.addLayout(vchild1_layout)
            right_layout.addLayout(header_layout)
            right_layout.addLayout(vchild3_layout)
            right_layout.addSpacing(5)

            # Splitter
            splitter = QSplitter(Qt.Orientation.Vertical)
            splitter.addWidget(QWidget())
            splitter.addWidget(QWidget())
            splitter.setSizes([self.height() // 5, self.height() * 4 // 5])
            splitter.setMidLineWidth(60)
            splitter.widget(1).setLayout(left_layout)
            splitter.widget(0).setLayout(QVBoxLayout())
            splitter.widget(0).layout().addWidget(newWidget)

            scroll_area = QScrollArea()
            scroll_area.setWidget(splitter)

            main_layout = QVBoxLayout()
            main_layout.addWidget(scroll_area)
            self.setLayout(main_layout)

            # Force window size
            self.resize(1000, self.sizeHint().height())
            print("init ui done")
        except Exception as e:
            print("there is problem in retrive data",e)
    
      
    def open_chat_room(self):
        if(Active.isLoggedIn):
            # Create a new instance of the Messenger class
            self.messenger_window = Messenger(self.user_data.get('username'))
            # Show the new window
            self.messenger_window.showFullScreen()
            self.messenger_window.show()
        else:
            QMessageBox.critical(self,"Error","Please Login First !!")

    def exit_page(self):
        self.close()
    
if __name__ == "__main__":
    try:
        app = QApplication(sys.argv)
        ex = UserInfo('namratachaudhari230')
        ex.setWindowTitle('User Info')
        
        ex.showFullScreen()
        ex.show()
        sys.exit(app.exec_())
    except Exception as e:
        print(f"An error occurred: {e}")
