import sys
from PyQt5 import QtCore, QtGui, QtWidgets

class ContactUsPage(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        # Create a scroll area
        scroll_area = QtWidgets.QScrollArea()
        scroll_area.setWidgetResizable(True)

        # Create a widget to contain the main layout
        scroll_widget = QtWidgets.QWidget()
        scroll_area.setWidget(scroll_widget)

        # Main layout for better organization
        main_layout = QtWidgets.QVBoxLayout(scroll_widget)

        # Visual elements
        self.setStyleSheet("background-color: #ffffff;")

        # Header
        self.add_header(main_layout)

        # Description
        self.add_description(main_layout)

        # Contact Information
        self.add_contact_info(main_layout)

        # Social Media Links
        self.add_social_media_links(main_layout)

        # Contact Form
        self.add_contact_form(main_layout)

        # Set layout for the scroll area
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(scroll_area)

        # Set window title and show the page
        self.setWindowTitle("Contact Us")
        self.show()

    def add_header(self, main_layout):
        header_layout = QtWidgets.QVBoxLayout()

        # Back button
        back_button = QtWidgets.QPushButton(QtGui.QIcon("assets/Mohit/backbt2.png"), "")
        back_button.setStyleSheet("border: none;")
        back_button.setFixedSize(50, 50)
        back_button.setIconSize(QtCore.QSize(30, 30))
        back_button.clicked.connect(self.go_back)
        header_layout.addWidget(back_button)

        # Logo and company name
        logo_label = QtWidgets.QLabel(self)
        logo_label.setPixmap(QtGui.QPixmap("assets/Mohit/contact.jpg").scaled(400, 200, QtCore.Qt.KeepAspectRatio))
        logo_label.setStyleSheet("margin:0px")
        logo_label.setAlignment(QtCore.Qt.AlignCenter)
        header_layout.addWidget(logo_label)

        company_name_label = QtWidgets.QLabel("Contact Us")
        company_name_label.setStyleSheet("font-size: 24px; font-weight: bold; color: #333;")
        company_name_label.setAlignment(QtCore.Qt.AlignCenter)
        header_layout.addWidget(company_name_label)

        # Add header layout to main layout
        main_layout.addLayout(header_layout)

    def add_description(self, main_layout):
        description_label = QtWidgets.QLabel("Welcome to our Contact Us page! We'd love to hear from you.")
        description_label.setStyleSheet("font-size: 16px; color:#d2691e;margin:5px")
        description_label.setAlignment(QtCore.Qt.AlignCenter)
        main_layout.addWidget(description_label)

    def add_contact_info(self, main_layout):
        location_layout = QtWidgets.QHBoxLayout()
        location_layout.setContentsMargins(0,0,0,0)
        location_layout.setAlignment(QtCore.Qt.AlignLeft)
        
        location_label = QtWidgets.QLabel("Location :")
        location_label.setStyleSheet("font-size: 18px; color: #333;margin:5px")
        
        location_button = QtWidgets.QPushButton(QtGui.QIcon("assets/Mohit/location.png"), "")
        location_button.setStyleSheet("border-radius: 10px")
        location_button.setFixedSize(50,50)
        location_button.setIconSize(QtCore.QSize(30, 30))   
        location_button.clicked.connect(self.open_location)

        location_layout.addWidget(location_label)
        location_layout.addWidget(location_button)

        main_layout.addLayout(location_layout)

    def add_social_media_links(self, main_layout):
        social_media_label = QtWidgets.QLabel("<b>Connect with Us</b>")
        social_media_label.setStyleSheet("font-size: 16px; color: #333;margin:5px")
        main_layout.addWidget(social_media_label)

        social_media_layout = QtWidgets.QHBoxLayout()
        social_media_layout.setContentsMargins(0,0,0,0)
        social_media_layout.setAlignment(QtCore.Qt.AlignLeft)

        # Phone Button
        phone_button = QtWidgets.QPushButton(QtGui.QIcon("assets/Mohit/phone.png"), "")
        phone_button.setStyleSheet("border-radius: 10px")
        phone_button.setFixedSize(50,50)
        phone_button.setIconSize(QtCore.QSize(30, 30))   
        phone_button.clicked.connect(self.open_phone)
        social_media_layout.addWidget(phone_button)

        # Email Button
        email_button = QtWidgets.QPushButton(QtGui.QIcon("assets/Mohit/email.png"), "")
        email_button.setStyleSheet("border-radius: 10px")
        email_button.setFixedSize(50,50)
        email_button.setIconSize(QtCore.QSize(30, 30))   
        email_button.clicked.connect(self.open_email)
        social_media_layout.addWidget(email_button)

        # Facebook Button
        facebook_button = QtWidgets.QPushButton(QtGui.QIcon("assets/Mohit/fb.png"), "")
        facebook_button.setStyleSheet("border-radius: 10px")
        facebook_button.setFixedSize(50,50)
        facebook_button.setIconSize(QtCore.QSize(30, 30))   
        facebook_button.clicked.connect(self.open_facebook)
        social_media_layout.addWidget(facebook_button)

        # Instagram Button
        instagram_button = QtWidgets.QPushButton(QtGui.QIcon("assets/Mohit/insta.jpeg"), "")
        instagram_button.setStyleSheet("border-radius: 10px")
        instagram_button.setFixedSize(50,50)
        instagram_button.setIconSize(QtCore.QSize(30, 30))
        instagram_button.clicked.connect(self.open_instagram)
        social_media_layout.addWidget(instagram_button)

        # LinkedIn Button
        linkedin_button = QtWidgets.QPushButton(QtGui.QIcon("assets/Mohit/linkedin.png"), "")
        linkedin_button.setStyleSheet("border-radius: 10px")
        linkedin_button.setFixedSize(50,50)
        linkedin_button.setIconSize(QtCore.QSize(30, 30))
        linkedin_button.clicked.connect(self.open_linkedin)
        social_media_layout.addWidget(linkedin_button)

        main_layout.addLayout(social_media_layout)

    def add_contact_form(self, main_layout):
        form_label = QtWidgets.QLabel("<b> Give us feedback :</b>")
        form_label.setStyleSheet("font-size: 18px; font-weight: bold; color: #333;margin:10px")
        main_layout.addWidget(form_label)

        form_layout = QtWidgets.QFormLayout()

        name_label = QtWidgets.QLabel("Name:")
        name_label.setStyleSheet("font-size:14px;")
        name_input = QtWidgets.QLineEdit()
        name_input.setStyleSheet("padding: 5px ;")
        name_input.setPlaceholderText("Enter your text here...")
        
        email_label = QtWidgets.QLabel("Email:")
        email_label.setStyleSheet("font-size:14px")
        email_input = QtWidgets.QLineEdit()
        email_input.setStyleSheet("padding: 5px ;")
        email_input.setPlaceholderText("Enter your email here...")
        
        message_label = QtWidgets.QLabel("Message:")
        message_input = QtWidgets.QTextEdit()
        message_label.setStyleSheet("font-size:14px")
        message_input.setStyleSheet("padding: 5px ;font-size:16px")
        message_input.setPlaceholderText("Please add your feedback about our website... Your feedback is important for improving our service...")
        
        submit_button = QtWidgets.QPushButton("Submit")
        submit_button.setStyleSheet("background-color: #7D3C98; color: white; border: 2px solid #7D3C98; font-size: 16px; border-radius: 5px; padding: 5px 10px; box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5);")
        
        submit_layout = QtWidgets.QHBoxLayout()
        submit_layout.setAlignment(QtCore.Qt.AlignCenter)
        # submit_layout.addStretch(1)  # Add stretchable space to push the button to the right
        submit_layout.addWidget(submit_button)

        form_layout.addRow(name_label, name_input)
        form_layout.addRow(email_label, email_input)
        form_layout.addRow(message_label, message_input)
        form_layout.addRow(submit_layout)  # Add the submit button layout
        main_layout.addLayout(form_layout)

    def open_phone(self):
        url ="tel:+917757902185"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def open_email(self):
        url ="mailto:core2webinfo@gmail.com"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def open_location(self):
        url ="https://maps.app.goo.gl/3Fgaoehg83GXXdv58"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def open_facebook(self):
        url = "https://www.facebook.com/core2webtechnologies"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def open_instagram(self):
        url = "https://www.instagram.com/click_and_collab?igsh=OHU3OXBzNHlyeWc4"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def open_linkedin(self):
        url = "https://www.linkedin.com/"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def open_git(self):
        url = "https://gitlab.com/"
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def go_back(self):
        self.close()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = ContactUsPage()
    window.setGeometry(5, 25, 1350, 700)
    sys.exit(app.exec_())
