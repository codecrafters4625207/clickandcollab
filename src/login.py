import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import QApplication, QLabel, QLineEdit, QPushButton, QVBoxLayout, QHBoxLayout, QFormLayout, QFileDialog, QRadioButton, QWidget, QMessageBox, QSplitter
from PyQt5.QtGui import QPixmap
from sign_up import SignUp
from PyQt5.QtCore import Qt
from PyQt5.QtCore import Qt, QUrl
from integration import Retrive

from Home2 import SearchBar  # Import the Home module

class C2W_UserProfileForm(QWidget):
    def __init__(self):
        super().__init__()
        self.c2w_init_ui()
        
    def c2w_init_ui(self):
        header_label = QLabel('Login')
        header_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        header_label.setStyleSheet('color: #333;padding: 10px;margin:50px; font-size: 40px; max-height:50px;')

        self.c2w_username_edit = QLineEdit()
        self.c2w_username_edit.setFixedWidth(200)
        self.c2w_username_edit.setFixedHeight(28)
        self.c2w_username_edit.setStyleSheet("border:2px solid black")

        self.c2w_password_edit = QLineEdit()
        self.c2w_password_edit.setFixedWidth(200)
        self.c2w_password_edit.setFixedHeight(28)
        self.c2w_password_edit.setStyleSheet("border:2px solid black")

        self.c2w_submit_button = QPushButton('Login')
        self.c2w_submit_button.setFixedWidth(100)
        self.c2w_submit_button.clicked.connect(self.open_home_page)
        self.c2w_submit_button.setStyleSheet("margin: 1px;background-color: #7D3C98; color: white;border: 2px solid #7D3C98;font-size:60px; border-radius: 5px; padding: 5px 10px; font-size: 16px;")

        create_new_label = QLabel('Don\'t have an account? ''<a href="#">SignUp.</a>')
        create_new_label.setAlignment(Qt.AlignmentFlag.AlignTop)
        create_new_label.setOpenExternalLinks(False)
        create_new_label.setStyleSheet("color: blue; text-decoration: underline;font-size:16px;margin:0px")
        create_new_label.linkActivated.connect(self.c2w_open_sign_up_page)

        self.username_label = QLabel("Username : ")
        self.username_label.setStyleSheet("font-size: 24px")
        self.pwd_label = QLabel("Password : ")
        self.pwd_label.setStyleSheet("font-size: 24px")

        form_layout = QFormLayout()
        form_layout.addRow(self.username_label, self.c2w_username_edit)
        form_layout.addRow(self.pwd_label, self.c2w_password_edit)
        form_layout.addRow(QLabel(""), self.c2w_submit_button)
        form_layout.addRow(QLabel(""),create_new_label)
        form_layout.setSpacing(30)
        form_layout.setFormAlignment(Qt.AlignmentFlag.AlignTop)
        form_layout.setLabelAlignment(Qt.AlignmentFlag.AlignRight)
        form_layout.setHorizontalSpacing(30)

        left_layout = QVBoxLayout()
        left_layout.addWidget(header_label)
        left_layout.addLayout(form_layout)
        left_layout.addWidget(create_new_label)

        newWidget = QWidget()
        image_label = QLabel(newWidget)
        pixmap = QPixmap("assets/Mohit/login.jpeg")
        scaled_pixmap = pixmap.scaled(450, 1000, Qt.AspectRatioMode.KeepAspectRatio)
        image_label.setPixmap(scaled_pixmap)
        image_layout = QVBoxLayout(newWidget)
        image_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)
        image_layout.addWidget(image_label)

        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(QWidget())
        splitter.addWidget(QWidget())
        splitter.setSizes([self.width() // 2, self.width() // 2])
        splitter.widget(1).setLayout(left_layout)
        splitter.widget(0).setLayout(QVBoxLayout())
        splitter.widget(0).layout().addWidget(newWidget)
        splitter.widget(1).setStyleSheet("background-color:#FFFFFF;border-radius:6px;")
        splitter.widget(0).setStyleSheet("background-color:#000000;border-radius:6px;")

        main_layout = QVBoxLayout()
        main_layout.addWidget(splitter)
        self.setLayout(main_layout)

    def c2w_submit_form(self):
        username = self.c2w_username_edit.text()
        password = self.c2w_password_edit.text()
        if not username or not password:
            QMessageBox.critical(self, "Error", "Please fill in all required fields.")
            return False
        else:
            if(Retrive.retrieve_user_data(username)):
                print("Login Successfully")
                return True
            else :
                QMessageBox.critical(self, "Error", "Please Enter valid Username or Password")
                return False
    
    def open_home_page(self):
        if(self.c2w_submit_form()):
            # Open the Home page
            self.home_window = SearchBar()
            self.home_window.show()  # Show the Home page

    def c2w_open_sign_up_page(self, url):
        self.sign_up_window = SignUp()
        self.sign_up_window.setGeometry(self.geometry())  # Set geometry to match login window
        self.sign_up_window.show()

   

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = C2W_UserProfileForm()
    ex.setWindowTitle('User Info')
    ex.setGeometry(20, 40, 1000, 700)
    ex.show()
    sys.exit(app.exec_())
