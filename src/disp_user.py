from integration import db

class Query:
    def __init__(self):
        # Query documents where profession is "FullStack Developer"
        self.query1 = db.collection('users').where('profession', '==', 'FullStack Developer').stream()

        # Query documents where profession is "FullStack Developer" or "Frontend Developer"
        self.query3 = db.collection('users').where('profession', 'in', ['FullStack Developer', 'Frontend Developer']).stream()

        # Query documents where profession is "FullStack Developer"
        self.query2 = db.collection('users').where('profession', 'in', ['FullStack Developer', 'Backend Developer']).stream()
