import sys
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QWidget, QVBoxLayout, QLabel, QPushButton, QScrollArea, QGridLayout
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import QSize, Qt
import firebase_admin
from firebase_admin import credentials, firestore
from integration import db,Active

# Initialize Firebase
'''
try:
    cred = credentials.Certificate("setup/clickandcollab-d6759-firebase-adminsdk-47m6g-97534fb6a3.json")  # Path to your service account key JSON file
    firebase_admin.initialize_app(cred)
    db = firestore.client()
except Exception as e:
    print("Firebase initialization failed:", e)
    sys.exit(1)
'''
class UserCard(QWidget):
    def __init__(self, data):
        print("I am in userdata")
        super().__init__()
        self.setup_ui(data)

    def setup_ui(self, name):
        doc_ref = db.collection('users').document(name)
        data = doc_ref.get() 
        print('data getted')
        layout = QVBoxLayout()
        layout.setSpacing(0)
        self.setStyleSheet("background-color: #7D3C98")  # Set background color for the entire card

        # Profile Image
        img_label = QLabel()
        img = data.get('profile_img')
        if img is None:
            img = "assets/profile/default_men.jpg" if data.get('gender') == 'Male' else "assets/profile/default_lady.jpg"
        pixmap = QPixmap(img)
        img_label.setPixmap(pixmap.scaled(90, 90, Qt.KeepAspectRatio, Qt.SmoothTransformation))
        img_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        img_label.setMargin(10)
        layout.addWidget(img_label)
        print("Does not have error in profile img")
        # Full Name
        full_name_label = QLabel(data.get('fullname'))
        print(full_name_label.text())
        full_name_label.setStyleSheet("color: black; font-size: 18px; font-weight: bold;")
        full_name_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        layout.addWidget(full_name_label)
        
        # Username
        username_label = QLabel(data.get('username'))
        username_label.setStyleSheet("color: black; font-size: 14px;")
        username_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        layout.addWidget(username_label)
        
        # Profession
        prof_label = QLabel(data.get('profession'))
        prof_label.setStyleSheet("color: black; font-size: 14px;")
        prof_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        layout.addWidget(prof_label)
        
        self.setLayout(layout)
        self.setMaximumSize(300, 400)
class FavoritePage(QWidget):
    def __init__(self):
        super().__init__()
        print("I am in favorites")
        self.setWindowTitle("Favorite")
        
        self.setStyleSheet("background-color: white;")
        self.setup_ui()

    def setup_ui(self):
        main_layout = QVBoxLayout(self)

        # Title Label
        title_layout=QHBoxLayout()
        title_label = QLabel("Favorite")
        title_label.setStyleSheet("color: white;font-size: 24px; font-weight: bold;font-family: Comic Sans MS;")
        title_label.setAlignment(Qt.AlignmentFlag.AlignLeft)
        #title_label.setContentsMargins(150,2,100,2)
        
        icon1 = QIcon("assets/Icons/left-arrow-removebg-preview.png")
        back_button=QPushButton()
        #back_button.setStyleSheet("background-color: black;")
        back_button.setIcon(icon1)
        back_button.setIconSize(QSize(50,40))
        #back_button.setContentsMargins(4,2,150,2)
        #back_button.setFixedSize(60,60)
        back_button.clicked.connect(self.exitPage)
        
        #back_button.setFixedSize(100,80)
        title_layout.addWidget(back_button)
        title_layout.addWidget(title_label)
        
        titleWid=QWidget()
        titleWid.setStyleSheet("background-color:black")
        titleWid.setLayout(title_layout)
        main_layout.addWidget(titleWid)
        # Fetch data from Firebase
        favorites_ref = db.collection('favorites')
        favorites = favorites_ref.get()

        if not favorites:
            # Display message if favorites list is empty
            no_favorites_label = QLabel("No one is in the favorite list.")
            no_favorites_label.setAlignment(Qt.AlignCenter)
            main_layout.addWidget(no_favorites_label)
        else:
            # Scroll Area for People
            scroll_area = QScrollArea()
            scroll_area.setWidgetResizable(True)
            scroll_widget = QWidget()
            scroll_widget.setStyleSheet("background-color:black;")
            scroll_layout = QGridLayout(scroll_widget)

            try:
                # Check if the "favorites" collection exists
                favorites_ref = db.collection('favorites').document(Active.activeUser)
                if favorites_ref.get().exists:
                    # Retrieve the document data as a dictionary
                    favorites_data = favorites_ref.get().to_dict()
                    # Get the field names (keys) from the dictionary
                    field_names = list(favorites_data.keys())
                    print("Field Names:", field_names)
                    for i, field in enumerate(field_names):
                        user_card = UserCard(field)
                        scroll_layout.addWidget(user_card, i // 2, i % 2)

                else:
                    print("Document does not exist.")
            except Exception as e:
                print(f"An error occurred in get_field_names: {e}")
            '''
            for i, favorite in enumerate(favorites):
                data = favorite.to_dict()

                # Create User Card
                user_card = UserCard(data)
            
                scroll_layout.addWidget(user_card, i // 2, i % 2)
            '''
            scroll_widget.setLayout(scroll_layout)
            scroll_area.setWidget(scroll_widget)
            main_layout.addWidget(scroll_area)

    def exitPage(self):
        self.close()

if __name__ == "__main__":
    
    app = QApplication(sys.argv)
    favorite_page = FavoritePage()
    favorite_page.show()
    sys.exit(app.exec_())
