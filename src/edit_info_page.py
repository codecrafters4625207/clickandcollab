import sys
from PyQt5.QtWidgets import QApplication, QFormLayout, QLabel, QLineEdit, QPushButton, QVBoxLayout, QHBoxLayout, QWidget, QSplitter, QScrollArea
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import QSize, Qt, QPropertyAnimation, QRect, QEasingCurve
from integration import db, Info, Active



class EditUserInfoPage(QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def save_changes(self):
        try:
            new_values = {
                'username': self.username_edit.text(),
                'email': self.email_edit.text(),
                'password': self.password_edit.text(),
                'fullname': self.fullname_edit.text(),
                'mobile': self.mobile_edit.text(),
                'profile_img': self.profile_img_edit.text(),
                'gender': self.gender_edit.text(),
                'description': self.description_edit.text(),
                'profession': self.profession_edit.text(),
                'skills': self.skills_edit.text(),
                'company': self.company_edit.text(),
                'experience': self.experience_edit.text(),
                'linkedin': self.linkedin_edit.text(),
                'gitlab': self.gitlab_edit.text(),
                'instagram': self.instagram_edit.text(),
                'facebook': self.facebook_edit.text(),
            }
        except Exception as e:
            print(e)

        try:
            # Get the reference to the Firestore document for the active user
            doc_ref = db.collection('users').document(Active.activeUser)
            
            # Update the document with the new values
            doc_ref.update(new_values)
            
            print("Data updated successfully!")
            self.accept()  # Close the dialog after saving changes
        except Exception as e:
            print("Failed to update data:", e)

    def init_ui(self):
        try:
            print("Here1")
            doc_ref = db.collection('users').document(Active.activeUser)
            try:
                data = doc_ref.get().to_dict()
            except Exception as e:
                print(e)

            # Create labels and line edits for each field
            username_label = QLabel("Username:")
            username_label.setStyleSheet("font-size: 24px;")
            self.username_edit = QLineEdit(data.get('username'))
            self.username_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            fullname_label = QLabel("Full Name:")
            fullname_label.setStyleSheet("font-size: 24px;")
            self.fullname_edit = QLineEdit(data.get('fullname'))
            self.fullname_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            email_label = QLabel("Email:")
            email_label.setStyleSheet("font-size: 24px;")
            self.email_edit = QLineEdit(data.get('email'))
            self.email_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            mobile_label = QLabel("Mobile No:")
            mobile_label.setStyleSheet("font-size: 24px;")
            self.mobile_edit = QLineEdit(data.get('mobile'))
            self.mobile_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            description_label = QLabel("Description:")
            description_label.setStyleSheet("font-size: 24px;")
            self.description_edit = QLineEdit(data.get('description'))
            self.description_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            profession_label = QLabel("Profession:")
            profession_label.setStyleSheet("font-size: 24px;")
            self.profession_edit = QLineEdit(data.get('profession'))
            self.profession_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            skills_label = QLabel("Skills:")
            skills_label.setStyleSheet("font-size: 24px;")
            self.skills_edit = QLineEdit(data.get('skills'))
            self.skills_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            company_label = QLabel("Current Company:")
            company_label.setStyleSheet("font-size: 24px;")
            self.company_edit = QLineEdit(data.get('company'))
            self.company_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            experience_label = QLabel("Total Experience:")
            experience_label.setStyleSheet("font-size: 24px;")
            self.experience_edit = QLineEdit(data.get('experience'))
            self.experience_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            linkedin_label = QLabel("LinkedIn:")
            linkedin_label.setStyleSheet("font-size: 24px;")
            self.linkedin_edit = QLineEdit(data.get('linkedin'))
            self.linkedin_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            gitlab_label = QLabel("GitLab:")
            gitlab_label.setStyleSheet("font-size: 24px;")
            self.gitlab_edit = QLineEdit(data.get('gitlab'))
            self.gitlab_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            instagram_label = QLabel("Instagram:")
            instagram_label.setStyleSheet("font-size: 24px;")
            self.instagram_edit = QLineEdit(data.get('instagram'))
            self.instagram_edit.setStyleSheet("font-size: 16px; padding: 5px;")

            facebook_label = QLabel("Facebook:")
            facebook_label.setStyleSheet("font-size: 24px;")
            self.facebook_edit = QLineEdit(data.get('facebook'))
            self.facebook_edit.setStyleSheet("font-size: 16px; padding: 5px;")
            print("Here2")

            # Create Save button
            self.save_button = QPushButton("Save")
            self.save_button.setFixedWidth(100)
            self.save_button.setStyleSheet("background-color: #7D3C98; color: white;border: 2px solid #7D3C98;font-size:60px; border-radius: 5px; padding: 5px 10px; font-size: 16px;")

            # create label
            name1 = QLabel("Personal ")
            name2 = QLabel("Professional ")
            name3 = QLabel("Links ")

            name1.setStyleSheet("font-size:24px;font-weight:bold")
            name2.setStyleSheet("font-size:24px;font-weight:bold")
            name3.setStyleSheet("font-size:24px;font-weight:bold")

            # create vbox for forms
            form_layout = QVBoxLayout()

            # Create layout for form
            first_form_layout = QFormLayout()

            first_form_layout.addRow(username_label, self.username_edit),
            first_form_layout.addRow(fullname_label, self.fullname_edit),
            first_form_layout.addRow(email_label, self.email_edit),
            first_form_layout.addRow(mobile_label, self.mobile_edit),
            first_form_layout.addRow(description_label, self.description_edit),
            first_form_layout.addRow(QLabel(""), QLabel(""))

            second_form_layout = QFormLayout()
            second_form_layout.addRow(profession_label, self.profession_edit),
            second_form_layout.addRow(skills_label, self.skills_edit),
            second_form_layout.addRow(company_label, self.company_edit),
            second_form_layout.addRow(experience_label, self.experience_edit),

            third_form_layout = QFormLayout()
            third_form_layout.addRow(linkedin_label, self.linkedin_edit),
            third_form_layout.addRow(gitlab_label, self.gitlab_edit),
            third_form_layout.addRow(instagram_label, self.instagram_edit),
            third_form_layout.addRow(facebook_label, self.facebook_edit)
            third_form_layout.addRow(QLabel(""), QLabel(""))
            third_form_layout.addRow(QLabel(""), self.save_button)

            form_layout.setSpacing(10)  # Adjust spacing between each line
            form_layout.addWidget(name1)
            form_layout.addLayout(first_form_layout)
            form_layout.addWidget(name2)
            form_layout.addLayout(second_form_layout)
            form_layout.addWidget(name3)
            form_layout.addLayout(third_form_layout)

            form_layout.setContentsMargins(20, 10, 20, 10)
            print("here3")
            # Set layout for this widget
            self.setLayout(form_layout)
        except Exception as e:
            print(e)


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        try:
            self.setWindowTitle("Main Window")
            self.setGeometry(20, 40, 1000, 700)
            # Create EditUserInfoPage widget
            edit_page = EditUserInfoPage()

            # Create scroll area for left side
            left_scroll_area = QScrollArea()
            left_scroll_area.setWidgetResizable(True)
            left_scroll_area.setWidget(edit_page)

            # Create back button for right side
            back_button = QPushButton("Back")
            back_button.setStyleSheet(
                "background-color: #7D3C98; color: white;border: 2px solid #7D3C98;font-size:60px; border-radius: 5px; padding: 5px 10px; font-size: 16px; ")
            back_button.setFixedSize(90, 30)
            back_button.clicked.connect(self.exit_page)

            # Load image
            image_label = QLabel()
            pixmap = QPixmap("assets/hand_shake.png")  # Specify your image path here

            scaled_edit_img = pixmap.scaled(400, 1000, Qt.AspectRatioMode.KeepAspectRatio)  # Adjust the width and height as needed
            image_label.setPixmap(scaled_edit_img)
            image_label.setContentsMargins(0, 0, 90, 0)

            # Create layout for right side
            right_layout = QVBoxLayout()
            right_layout.addWidget(image_label)
            # Create horizontal layout for back button
            back_button_layout = QHBoxLayout()
            back_button_layout.addStretch()  # Add stretchable space before the button
            back_button_layout.addWidget(back_button)
            back_button_layout.addStretch()  # Add stretchable space after the button

            right_layout.addLayout(back_button_layout)  # Add the horizontal layout to the right layout

            # Create scroll area for right side
            right_widget = QWidget()
            right_widget.setStyleSheet("background-color:black")
            right_widget.setLayout(right_layout)

            right_scroll_area = QScrollArea()
            right_scroll_area.setWidgetResizable(True)
            right_scroll_area.setWidget(right_widget)

            # Create splitter to divide the screen into two sides
            splitter = QSplitter(Qt.Orientation.Horizontal)
            splitter.addWidget(left_scroll_area)
            splitter.addWidget(right_scroll_area)
            splitter.setSizes([self.width() * 2 // 3, self.width() // 3])

            # Set layout for the main window
            main_layout = QHBoxLayout()
            main_layout.addWidget(splitter)
            self.setLayout(main_layout)
        except Exception as e:
            print(e)

    def exit_page(self):
        self.close()


if __name__ == "__main__":
    try:
        app = QApplication(sys.argv)
        main_window = MainWindow()
        main_window.show()
        sys.exit(app.exec_())
    except Exception as e:
        print("An exception occurred:", e)
