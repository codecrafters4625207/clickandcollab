import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QLabel, QLineEdit, QPushButton,QVBoxLayout, QHBoxLayout, QFormLayout, QFileDialog, QRadioButton, QWidget,QMessageBox, QScrollArea, QSplitter
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
from integration import Info,Integration
class SignUp(QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()
    def init_ui(self):
        header_label = QLabel('Sign Up')
        header_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        header_label.setStyleSheet('color: #333;font-weight:bold;padding: 10px; margin:50px;font-size: 40px; max-height:50px;text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.5);')
        

        self.username_edit = QLineEdit()
        self.username_edit.setFixedWidth(200)
        self.username_edit.setFixedHeight(28)
        self.username_edit.setStyleSheet("border:2px solid #7D3C98;font-size:16px;")
        self.fullname_edit = QLineEdit()
        self.fullname_edit.setFixedWidth(200)
        self.fullname_edit.setFixedHeight(28)
        self.fullname_edit.setStyleSheet("border:2px solid #7D3C98")
        self.set_password_edit = QLineEdit()
        self.set_password_edit.setFixedWidth(200)
        self.set_password_edit.setFixedHeight(28)
        self.set_password_edit.setStyleSheet("border:2px solid #7D3C98")
        self.retype_password_edit = QLineEdit()
        self.retype_password_edit.setFixedWidth(200)
        self.retype_password_edit.setFixedHeight(28)
        self.retype_password_edit.setStyleSheet("border:2px solid #7D3C98")
        self.email_edit = QLineEdit()
        self.email_edit.setFixedWidth(200)
        self.email_edit.setFixedHeight(28)
        self.email_edit.setStyleSheet("border:2px solid #7D3C98")
        self.mobile_no_edit = QLineEdit()
        self.mobile_no_edit.setFixedWidth(200)
        self.mobile_no_edit.setFixedHeight(28)
        self.mobile_no_edit.setStyleSheet("border:2px solid #7D3C98")

        self.register_button = QPushButton('Register')
        self.register_button.setFixedWidth(100)
        self.register_button.setStyleSheet("margin: 1px;background-color: #7D3C98; color: white;border: 2px solid #7D3C98;font-size:60px; border-radius: 5px; padding: 5px 10px; font-size: 16px; box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);")
        self.register_button.clicked.connect(self.save_to_variables)
        
        self.username_label=QLabel("Username : ")
        self.username_label.setStyleSheet("font-size: 24px")

        self.fullname_label=QLabel("Full Name : ")
        self.fullname_label.setStyleSheet("font-size: 24px")

        self.set_pwd_label=QLabel("Set-Password : ")
        self.set_pwd_label.setStyleSheet("font-size: 24px")

        self.retype_pwd_label=QLabel("Re-type : ")
        self.retype_pwd_label.setStyleSheet("font-size: 24px")

        self.email_label=QLabel("Email Id : ")
        self.email_label.setStyleSheet("font-size: 24px")

        self.mob_no_label=QLabel("Mobile No : ")
        self.mob_no_label.setStyleSheet("font-size: 24px")

        #Radio btns
        self.gender_label = QLabel("Gender:")
        self.gender_label.setStyleSheet("font-size: 24px")

        self.gender_female_radio = QRadioButton("Female")
        self.gender_female_radio.setStyleSheet(
            "QRadioButton {"
            "   font-size: 16px;"
            "}"
            "QRadioButton::indicator {"
            "   width: 16px;"
            "   height: 16px;"
            "   border-radius: 10px;"
            "   border: 2px solid #7D3C98;"  # Outer border color
            #"   background-color: #ddd;"  # Unchecked background color
            "}"
            "QRadioButton::indicator:checked {"
            "   background-color: #7D3C98;"  # Checked background color
            "   border: 2px solid #7D3C98;"  # Border color when checked
            "}"
        )

        self.gender_male_radio = QRadioButton("Male")
        self.gender_male_radio.setStyleSheet(
            "QRadioButton {"
            "   font-size: 16px;"
            "}"
            "QRadioButton::indicator {"
            "   width: 16px;"
            "   height: 16px;"
            "   border-radius: 10px;"
            "   border: 2px solid #7D3C98;"  # Outer border color
            #"   background-color: #ddd;"  # Unchecked background color
            "}"
            "QRadioButton::indicator:checked {"
            "   background-color: #7D3C98;"  # Checked background color
            "   border: 2px solid #7D3C98;"  # Border color when checked
            "}"
        )
        self.gender_female_radio.clicked.connect(self.set_female_gender)
        self.gender_male_radio.clicked.connect(self.set_male_gender)

        # Layout for gender selection
        gender_layout = QHBoxLayout()
        gender_layout.addWidget(self.gender_female_radio)
        gender_layout.addWidget(self.gender_male_radio)

        form_layout = QFormLayout()

        form_layout.addRow(self.username_label, self.username_edit)
        form_layout.addRow(self.fullname_label, self.fullname_edit)
        form_layout.addRow(self.set_pwd_label, self.set_password_edit)
        form_layout.addRow(self.retype_pwd_label, self.retype_password_edit)
        form_layout.addRow(self.email_label, self.email_edit)
        form_layout.addRow(self.mob_no_label, self.mobile_no_edit)
        form_layout.addRow(self.gender_label, gender_layout)
        form_layout.addRow(QLabel(""))
        form_layout.addRow(QLabel(""),self.register_button)
        form_layout.setSpacing(10)
        form_layout.setFormAlignment(Qt.AlignmentFlag.AlignTop)
        button_layout = QVBoxLayout()
        form_layout.setContentsMargins(40,10,0,0)
        
        create_new_label = QLabel('Already have an account? ''<a href="#">Login.</a>')
        create_new_label.setAlignment(Qt.AlignmentFlag.AlignTop)
        create_new_label.setOpenExternalLinks(False)
        create_new_label.setStyleSheet("color: blue;margin-right:0px; margin-left:0px;text-decoration: underline;font-size:16px")
        create_new_label.linkActivated.connect(self.exit)

        login_layout=QHBoxLayout()
        login_layout.addWidget(create_new_label)

        left_layout = QVBoxLayout()
        left_layout.addWidget(header_label)
        left_layout.addLayout(form_layout)
        left_layout.addLayout(button_layout)
        left_layout.addLayout(login_layout)

        self.records_widget = QWidget()  # Widget to hold the records layout
        self.records_layout = QVBoxLayout(self.records_widget)  # QVBoxLayout for the records
        self.records_layout.setAlignment(Qt.AlignmentFlag.AlignTop)  # Align records to the top

        right_layout=QVBoxLayout()
        image_label = QLabel()
        pixmap = QPixmap("assets/profile_lady.png")  # Replace "your_image_path.jpg" with the path to your image
        scaled_pixmap = pixmap.scaled(450, 1000, Qt.AspectRatioMode.KeepAspectRatio)  # Adjust the width and height as needed
        image_label.setPixmap(scaled_pixmap)
        image_label.setAlignment(Qt.AlignmentFlag.AlignRight)

        
        right_layout.addWidget(image_label)
        right_layout.setContentsMargins(0,160,0,0)

        splitter = QSplitter(Qt.Orientation.Horizontal)
        splitter.addWidget(QWidget())
        splitter.addWidget(QWidget())
        splitter.setSizes([self.width() // 2, self.width() // 2])
        splitter.widget(0).setLayout(left_layout)
        splitter.widget(0).setStyleSheet("background-color:#FFFFFF;border-radius:6px;")
        splitter.widget(1).setStyleSheet("background-color:#000000;border-radius:6px;")
        splitter.widget(1).setLayout(right_layout)
        main_layout = QVBoxLayout()
        main_layout.addWidget(splitter)
        self.setLayout(main_layout)

    def exit(self):
        self.close()
    
    def set_female_gender(self):
        if self.gender_female_radio.isChecked():
            Info.gender = "Female"

    def set_male_gender(self):
        if self.gender_male_radio.isChecked():
            Info.gender = "Male"
    
    def save_to_variables(self):
        print("Clicked Register button")
        if self.is_form_valid():
            Info.username = self.username_edit.text()
            Info.full_name = self.fullname_edit.text()
            Info.email = self.email_edit.text()
            Info.password = self.set_password_edit.text()
            if Info.password != self.retype_password_edit.text():
                print("Passwords do not match")
                return
            Info.mobile_no = self.mobile_no_edit.text()
        
            if(Integration.sign_up()):
                QMessageBox.information(self,"Congratulations","Signing Successful")
            else:
                QMessageBox.warning(self,"Error!!","Existing User Please Login")
        else:
            QMessageBox.warning(self, "Warning", "Please fill in all the fields.")

    def is_form_valid(self):
        # Check if all fields are filled
        if (not self.username_edit.text() or
            not self.fullname_edit.text() or
            not self.email_edit.text() or
            not self.set_password_edit.text() or
            not self.retype_password_edit.text() or
            not self.mobile_no_edit.text()):
            return False
        return True
       

if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=SignUp()
    ex.setWindowTitle('User Info')
    ex.setGeometry(20,40,1000,700)
    ex.show()
    sys.exit(app.exec_())