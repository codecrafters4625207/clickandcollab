import sys
from PyQt5 import QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QPixmap, QColor
from PyQt5.QtWidgets import (QGraphicsDropShadowEffect, QHBoxLayout, QVBoxLayout, QLabel,
                             QPushButton, QLineEdit, QMainWindow, QApplication, QWidget,
                             QFrame, QScrollArea)

from login import C2W_UserProfileForm


class HomePage(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Home')
        self.setStyleSheet("background-color: #ffffff")
        self.setGeometry(20, 40, 100, 700)

        # Main widget
        main_widget = QWidget()
        main_layout = QVBoxLayout()
        main_widget.setLayout(main_layout)
        

        # Scroll area
        scroll_area = QScrollArea()
        scroll_widget = QWidget()
        scroll_layout = QVBoxLayout()
        scroll_area.setFixedSize(1000,600)
        scroll_widget.setLayout(scroll_layout)
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(scroll_widget)

        # Set vertical scroll policy
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

        # Appbar
        appbar = QHBoxLayout()  
        appbar.setAlignment(Qt.AlignmentFlag.AlignTop)

        logo_label = QLabel()
        pixmap = QPixmap('assets/images/logo.png')
        pixmap = pixmap.scaledToWidth(70)
        logo_label.setPixmap(pixmap)
        appbar.addWidget(logo_label)

        search_input = QLineEdit()
        search_input.setStyleSheet("border-radius: 15px;padding: 10px;border: 2px solid #0c0d0d;")
        search_input.setPlaceholderText("Enter your text here...")
        search_input.setFixedSize(400,40)
        appbar.addWidget(search_input)

        search_button = QPushButton('Search')
        search_button.setStyleSheet("color: white; background-color: black; font-weight: bold; border-radius: 15px;padding: 5px; border: 2px solid #0c0d0d;")
        search_button.setFixedSize(100,40)

        appbar.addWidget(search_button)

        login_button = QPushButton('Login / SignUp')
        login_button.setStyleSheet("color: white; background-color: black; font-weight: bold; border-radius: 15px;padding: 10px;border: 2px solid #0c0d0d;")
        login_button.setFixedSize(150,50)
        login_button.clicked.connect(self.open_login)
        appbar.addWidget(login_button)

        main_layout.addLayout(appbar)
        main_layout.addWidget(scroll_area)

        # Add ShadowedImageWidget
        shadowed_image_widget = ShadowedImageWidget('assets/images/partnership.png')
        shadowed_image_widget.setFixedSize(930, 300)
        shadowed_image_widget.setStyleSheet("border-radius: 20px;background-color: #FCFCFC;")
        scroll_layout.addWidget(shadowed_image_widget)

        # Add first label
        label1 = QLabel('Together We Rise ')
        label1.setStyleSheet("color:black; font-size: 40px; font-weight: bold;padding: 0; background-color: None; border-radius: 20px; border-style: solid;padding: 13px")
        label1.setAlignment(Qt.AlignmentFlag.AlignRight)
        shadowed_image_widget.add_label(label1)

        # Add second label
        label2 = QLabel('Find your perfect partner based on interests, \n\t skills and location.')
        label2.setStyleSheet("color:black; font-size: 25px; font-weight: regular;background-color: None; border-radius: 20px; padding: 0; border-style: solid;padding: 13px")
        label2.setAlignment(Qt.AlignmentFlag.AlignRight)
        shadowed_image_widget.add_label(label2)

        # Frontend Developer
        dev1_label = QLabel("Frontend Developer")
        dev1_label.setStyleSheet("color: black; font-size: 20px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: white")
        dev1_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev1_label)
    
        # Add first row
        first_row_container = QWidget()
        first_row_layout = QHBoxLayout()
        first_row_container.setLayout(first_row_layout)

        # Load images and corresponding texts
        image_paths = ['assets/profile/image1.png', 'assets/profile/image2.png', 'assets/profile/image3.png', 'assets/profile/image4.png', 'assets/profile/image5.png', 'assets\Icons\image.png', 'assets/Icons/image.png', 'assets/Icons/image.png', 'assets/Icons/image.png', 'assets/Icons/image.png']
        names = ['Shashi Bagal', 'Sachin Patil', 'Govind Dindewad', 'Vishal Rebel', 'Rahul Hatkar', 'Rameshwari Kadrame', 'Sejal Hanmante', 'Akshay Lahane', 'Nikita Salunkhe', 'Sahil Bhandekar']
        description = ['bagalshashi','pail_sach.in', 'govinda_130', '_rebel_vishal', 'rahulhatkar','rameshwarikadrame123','sejalhanmante230','__ak__lahane','nikitasalunkhe12','sahilbhandekar']
        for i, (image_path, text1,text2) in enumerate(zip(image_paths, names,description), start=1):
            
            image_layout = QVBoxLayout()

            # Add the image
            label = QLabel()    
            pixmap = QPixmap(image_path)
            label.setPixmap(pixmap)
            label.setStyleSheet(" color: black; padding: 10px;border-radius: 5px;")
            label.setScaledContents(True)  
            label.setFixedSize(180, 180)  
            image_layout.addWidget(label)

            # Add the Names
            names_label = QLabel((text1 +  "<br>"  + text2))
            names_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
            names_label.setStyleSheet("background-color: #c8f9a2; color: black; padding: 5px; border-radius: 5px;font-weight: bold;font-size: 15px")
            names_label.setFixedSize(180, 50)  
            image_layout.addWidget(names_label)

            
            first_row_layout.addLayout(image_layout)


        first_row_scroll_area = QScrollArea()
        first_row_scroll_area.setWidgetResizable(True)
        first_row_scroll_area.setFixedSize(950, 280)
        first_row_scroll_area.setWidget(first_row_container)
        first_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)  
        first_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_layout.addWidget(first_row_scroll_area)
        
        # Backend Developer
        dev2_label = QLabel("Backend Developer")
        dev2_label.setStyleSheet("color: black; font-size: 20px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: white")
        dev2_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev2_label)
    
        # Add first row
        second_row_container = QWidget()
        second_row_layout = QHBoxLayout()
        second_row_container.setLayout(second_row_layout)

        # Load images and corresponding texts
        image_paths = ['assets/profile/namrata.png', 'assets/profile/nikitaM.png', 'assets/profile/mohit.png', 'assets/profile/om.png', 'assets/profile/rameshwari.png', 'assets/Icons/image.png', 'assets/Icons/image.png', 'assets/Icons/image.png', 'assets/Icons/image.png', 'assets/Icons/image.png']
        names = ['Namrata Chaudhari', 'Nikita Mundphan', 'Mohit Badgujar', 'Om Bhandarkar', 'Rameshwari Kadrame', 'Pratik Donajamath', 'Sejal Hanmante', 'Akshay Lahane', 'Nikita Salunkhe', 'Sahil Bhandekar']
        description = ['namratachaudhari230','_nikita_mundphan', 'mohitbadgujar45', 'ommie_1206', 'rameshwarikadrame123','sejalhanmante230','__ak__lahane','nikitasalunkhe12','sahilbhandekar']
        for i, (image_path, text1,text2) in enumerate(zip(image_paths, names, description), start=1):
            
            image_layout = QVBoxLayout()

            # Add the image
            label = QLabel()
            pixmap = QPixmap(image_path)
            label.setPixmap(pixmap)
            label.setStyleSheet("color: black; padding: 10px;border-radius: 5px;")
            label.setScaledContents(True)  
            label.setFixedSize(180, 180)  
            image_layout.addWidget(label)

            # Add the Names
            names_label = QLabel((text1 +  "\n"  + text2).replace("\n", "<br>"))
            names_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
            names_label.setStyleSheet("background-color: #c8f9a2; color: black; padding: 5px; border-radius: 5px;font-weight: bold;font-size: 15px")
            names_label.setFixedSize(180, 50)  
            image_layout.addWidget(names_label)
            
            second_row_layout.addLayout(image_layout)


        second_row_scroll_area = QScrollArea()
        second_row_scroll_area.setWidgetResizable(True)
        second_row_scroll_area.setFixedSize(950, 280)
        second_row_scroll_area.setWidget(second_row_container)
        second_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)  # Add this line to set horizontal scrollbar policy
        second_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_layout.addWidget(second_row_scroll_area)
        
        # Backend Developer
        dev3_label = QLabel("UIUX Designer")
        dev3_label.setStyleSheet("color: black; font-size: 20px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: white")
        dev3_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev3_label)
    
        # Add first row
        third_row_container = QWidget()
        third_row_layout = QHBoxLayout()
        third_row_container.setLayout(third_row_layout)

        # Load images and corresponding texts
        image_paths = ['assets/profile/image1.png', 'assets/profile/image2.png', 'assets/profile/image3.png', 'assets/profile/image4.png', 'assets/profile/image5.png', 'assets\Icons\image.png', 'assets/Icons/image.png', 'assets/Icons/image.png', 'assets/Icons/image.png', 'assets/Icons/image.png']
        names = ['Shashi Bagal', 'Sachin Patil', 'Govind Dindewad', 'Vishal Rebel', 'Rahul Hatkar', 'Rameshwari Kadrame', 'Sejal Hanmante', 'Akshay Lahane', 'Nikita Salunkhe', 'Sahil Bhandekar']
        description = ['bagalshashi','pail_sach.in', 'govinda_130', '_rebel_vishal', 'rahulhatkar','rameshwarikadrame123','sejalhanmante230','__ak__lahane','nikitasalunkhe12','sahilbhandekar']
        for i, (image_path, text1,text2) in enumerate(zip(image_paths, names,description), start=1):
            
            image_layout = QVBoxLayout()

            # Add the image
            label = QLabel()
            pixmap = QPixmap(image_path)
            label.setPixmap(pixmap)
            label.setStyleSheet("background-color: white; color: black; padding: 10px;border-radius: 5px;")
            label.setScaledContents(True)  
            label.setFixedSize(180, 180)  
            image_layout.addWidget(label)

            # Add the Names
            names_label = QLabel((text1 +  "<br>"  + text2))
            names_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
            names_label.setStyleSheet("background-color: #c8f9a2; color: black; padding: 5px; border-radius: 5px;font-weight: bold;font-size: 15px")
            names_label.setFixedSize(180, 50)  
            image_layout.addWidget(names_label)
            
            third_row_layout.addLayout(image_layout)

        third_row_scroll_area = QScrollArea()
        third_row_scroll_area.setWidgetResizable(True)
        third_row_scroll_area.setFixedSize(950, 280)
        third_row_scroll_area.setWidget(third_row_container)
        third_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)  # Add this line to set horizontal scrollbar policy
        third_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_layout.addWidget(third_row_scroll_area)

        copyright_label = QLabel("© Copyright 2024 Click&Collab. All rights reserved.")
        copyright_label.setStyleSheet("color: white; font-size: 20px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: black")
        copyright_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(copyright_label)

        self.setCentralWidget(main_widget)

    def open_login(self):
        self.login_page=C2W_UserProfileForm()
        self.login_page.setGeometry(self.geometry())
        self.login_page.show()

    def search(self):
        search_query = self.search_input.text()
        print("Search query:", search_query)


class ShadowedImageWidget(QLabel):
    def __init__(self, image_path, parent=None):
        super().__init__(parent)
        self.set_image_with_shadow(image_path)
        self.labels_layout = QVBoxLayout()
        self.labels_layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.setLayout(self.labels_layout)

    def set_image_with_shadow(self, image_path):
        pixmap = QPixmap(image_path)
        self.setPixmap(pixmap)
        shadow_effect = QGraphicsDropShadowEffect()
        shadow_effect.setBlurRadius(40)
        shadow_effect.setColor(QColor(0, 0, 0, 200))
        shadow_effect.setOffset(4, 4)

        self.setGraphicsEffect(shadow_effect)

    def add_label(self, label):
        self.labels_layout.addWidget(label)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    search_bar = HomePage()
    search_bar.setWindowIcon(QtGui.QIcon('F:\\Code_Crafter\\Demo\\logo.png'))
    search_bar.show()
    sys.exit(app.exec_())
