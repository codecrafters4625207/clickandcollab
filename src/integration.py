import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QMessageBox
import firebase_admin
from firebase_admin import credentials, firestore

class Active:
    activeUser="namratachaudhari230"
    isLoggedIn=False

class DB:
    @classmethod
    def getdb(cls):
        cred = credentials.Certificate("setup/clickandcollab-d6759-firebase-adminsdk-47m6g-97534fb6a3.json")  # Path to your Firebase Admin SDK key
        firebase_admin.initialize_app(cred)
        db = firestore.client()
        return db

db=DB.getdb()
class Retrive:
    username=None
    email=None
    password=None
    full_name=None
    mobile_no=None
    profile_img=None
    gender=None
    description=None
    profession=None
    skills=None
    current_company=None
    experience=None
    linkdin=None
    gitlab=None
    instagram=None
    facebook=None

    @classmethod
    def retrieve_user_data(cls, uname):
        try:
            user_ref = db.collection('users').document(uname).get()
            if user_ref.exists:
                user_data = user_ref.to_dict()
                Retrive.username=user_data.get('username')
                Retrive.email=user_data.get('email')
                Retrive.password="Password:", user_data.get('password')
                Retrive.full_name=user_data.get('full_name')
                Retrive.mobile_no=user_data.get('mobile')
                Retrive.profile_img=user_data.get('profile_img')
                Retrive.gender=user_data.get('gender')
                Retrive.description=user_data.get('description')
                Retrive.profession=user_data.get('profession')
                Retrive.skills=user_data.get('skills')
                Retrive.current_company=user_data.get('current_company')
                Retrive.experience="Experience:", user_data.get('experience')
                Retrive.linkdin="LinkedIn:", user_data.get('linkdin')
                Retrive.gitlab="GitLab:", user_data.get('gitlab')
                Retrive.instagram="Instagram:", user_data.get('instagram')
                Retrive.facebook="Facebook:", user_data.get('facebook')
                return True
            else:
                print(f"User '{uname}' not found.")
                return False
        except Exception as e:
            print("Error retrieving user data:", e)


    

class Info:
    username=None
    email=None
    password=None
    full_name=None
    mobile_no=None
    profile_img=None
    gender=None
    description=None
    profession=None
    skills=None
    current_company=None
    experience=None
    linkdin=None
    gitlab=None
    instagram=None
    facebook=None
    no_star=2
    gain_star=5



class Integration:
  
    
    @classmethod
    def submit_data(cls):
        try:
            data = {
                'username': Info.username,
                'email': Info.email,
                'password': Info.password,
                'fullname': Info.full_name,
                'mobile': Info.mobile_no,
                'profile_img': Info.profile_img,
                'gender': Info.gender,
                'description': Info.description,
                'profession': Info.profession,
                'skills': Info.skills,
                'company': Info.current_company,
                'experience': Info.experience,
                'linkedin': Info.linkdin,
                'gitlab': Info.gitlab,
                'instagram': Info.instagram,
                'facebook': Info.facebook,
                'no_star': 2,  # Initialize no_star to 0
                'gain_star': 5  # Initialize gain_star to 0
            }

            # Check if the document already exists
            user_ref = db.collection('users').document(Info.username).get()
            if user_ref.exists:
                # If the document exists, update it
                db.collection('users').document(Info.username).update(data)
                print('Data updated successfully!')
            else:
                # If the document doesn't exist, set it
                db.collection('users').document(Info.username).set(data)
                print('Data submitted successfully!')
           
        except Exception as e:
            print(f'Failed to submit/update data: {e}')
            return False  # Indicate failure
        return True
    
    
    
 
    @classmethod
    def check_uniqueness(cls, username, email):
        try:
            user_ref = db.collection('users').document(username).get()
            email_ref = db.collection('users').where('email', '==', email).get()
            return not (user_ref.exists or email_ref)
        except Exception as e:
            print(f'Failed to check uniqueness: {e}')
            return False  # Assume uniqueness check failed
        
    @classmethod
    def retrieve_all_users(cls):
        try:
            # Query all documents from 'users' collection
            users_ref = db.collection('users').stream()

            # Iterate over each document
            for user_doc in users_ref:
                # Extract data from document
                user_data = user_doc.to_dict()

                # Print user data
                print("Username:", user_data.get('username'))
                print("Email:", user_data.get('email'))
                print("Password:", user_data.get('password'))
                print("Full Name:", user_data.get('fullname'))
                print("Mobile:", user_data.get('mobile'))
                print("Profile Image:", user_data.get('profile_img'))
                print("Gender:", user_data.get('gender'))
                print("Description:", user_data.get('description'))
                print("Profession:", user_data.get('profession'))
                print("Skills:", user_data.get('skills'))
                print("Company:", user_data.get('company'))
                print("Experience:", user_data.get('experience'))
                print("LinkedIn:", user_data.get('linkedin'))
                print("GitLab:", user_data.get('gitlab'))
                print("Instagram:", user_data.get('instagram'))
                print("Facebook:", user_data.get('facebook'))
                print("\n")

        except Exception as e:
            print("Error retrieving users:", e)
    @classmethod
    def sign_up(cls):
        # Check for uniqueness of username and email
        if cls.check_uniqueness(Info.username, Info.email):
            # Proceed with sign-up process
            # Create a new user record
            data = {
                'username': Info.username,
                'email': Info.email,
                'password': Info.password,
                'fullname': Info.full_name,
                'mobile': Info.mobile_no,
                'profile_img': Info.profile_img,
                'gender': Info.gender,
                'description': Info.description,
                'profession': Info.profession,
                'skills': Info.skills,
                'company': Info.current_company,
                'experience': Info.experience,
                'linkedin': Info.linkdin,
                'gitlab': Info.gitlab,
                'instagram': Info.instagram,
                'facebook': Info.facebook
            }
            db.collection('users').document(Info.username).set(data)
            print('Sign up successful!')
            return True
        else:
            # Username or email already exists
            print('Username or Email already exists!')
            return False

    '''
    @classmethod
    def update_gender(cls, username, new_gender):
        try:
            # Query the user document based on the username
            user_ref = db.collection('users').document(username)

            # Update the gender field
            user_ref.update({'gender': new_gender})
        
            cls.logger.info(f"Gender updated successfully for {username}!")
            print("updated gender")
        except Exception as e:
            cls.logger.error(f"Failed to update gender for {username}: {e}")

        '''
#Integration.retrieve_all_users()
