import os
import sys
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QDialog, QLabel, QLineEdit, QPushButton,QVBoxLayout, QHBoxLayout, QFormLayout, QFileDialog, QRadioButton, QWidget,QMessageBox, QScrollArea, QSplitter
from PyQt5.QtGui import QColor, QPainter, QPainterPath, QPixmap
from PyQt5.QtCore import Qt
from integration import Info
from edit_info_page import EditUserInfoPage, MainWindow
from integration import db,Active


class EditProfileDialog(QDialog):
    def __init__(self, current_values, parent=None):
        
        super().__init__(parent)
        self.setWindowTitle("Edit Profile")
        self.resize(600, 600)
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)  # This line removes the window frame
        
        layout = QFormLayout()

        font = QtGui.QFont()
        font.setPointSize(10)

        self.username_edit = QLineEdit(current_values.get('username'))
        self.username_edit.setFont(font)
        self.email_edit = QLineEdit(current_values.get('email'))
        self.email_edit.setFont(font)
        self.password_edit = QLineEdit(current_values.get('password'))
        self.password_edit.setFont(font)
        self.fullname_edit = QLineEdit(current_values.get('fullname'))
        self.fullname_edit.setFont(font)
        self.mobile_edit = QLineEdit(current_values.get('mobile'))
        self.mobile_edit.setFont(font)
        self.profile_img_edit = QLineEdit(current_values.get('profile_img'))
        self.profile_img_edit.setFont(font)
        self.gender_edit = QLineEdit(current_values.get('gender'))
        self.gender_edit.setFont(font)
        self.description_edit = QLineEdit(current_values.get('description'))
        self.description_edit.setFont(font)
        self.profession_edit = QLineEdit(current_values.get('profession'))
        self.profession_edit.setFont(font)
        self.skills_edit = QLineEdit(current_values.get('skills'))
        self.skills_edit.setFont(font)
        self.company_edit = QLineEdit(current_values.get('company'))
        self.company_edit.setFont(font)
        self.experience_edit = QLineEdit(current_values.get('experience'))
        self.experience_edit.setFont(font)
        self.linkedin_edit = QLineEdit(current_values.get('linkedin'))
        self.linkedin_edit.setFont(font)
        self.gitlab_edit = QLineEdit(current_values.get('gitlab'))
        self.gitlab_edit.setFont(font)
        self.instagram_edit = QLineEdit(current_values.get('instagram'))
        self.instagram_edit.setFont(font)
        self.facebook_edit = QLineEdit(current_values.get('facebook'))
        self.facebook_edit.setFont(font)

        layout.addRow("Username:", self.username_edit)
        layout.addRow("Email:", self.email_edit)
        layout.addRow("Password:", self.password_edit)
        layout.addRow("Full Name:", self.fullname_edit)
        layout.addRow("Mobile:", self.mobile_edit)
        layout.addRow("Profile Image:", self.profile_img_edit)
        layout.addRow("Gender:", self.gender_edit)
        layout.addRow("Description:", self.description_edit)
        layout.addRow("Profession:", self.profession_edit)
        layout.addRow("Skills:", self.skills_edit)
        layout.addRow("Company:", self.company_edit)
        layout.addRow("Experience:", self.experience_edit)
        layout.addRow("Linkedin:", self.linkedin_edit)
        layout.addRow("Gitlab:", self.gitlab_edit)
        layout.addRow("Instagram:", self.instagram_edit)
        layout.addRow("Facebook:", self.facebook_edit)
        
        save_button = QPushButton("Save")
        save_button.clicked.connect(self.save_changes)
        save_button.setStyleSheet("background-color: #7D3C98; color:white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px;")
        layout.addWidget(save_button)

        back_button=QPushButton("Back")
        back_button.setStyleSheet("background-color: #7D3C98; color:white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px;")
        back_button.clicked.connect(self.exitPage)
        layout.addWidget(back_button)
        self.setLayout(layout)
    
    def exitPage(self):
        self.reject()

    def save_changes(self):
        new_values = {
            'username': self.username_edit.text(),
            'email': self.email_edit.text(),
            'password': self.password_edit.text(),
            'fullname': self.fullname_edit.text(),
            'mobile': self.mobile_edit.text(),
            'profile_img': self.profile_img_edit.text(),
            'gender': self.gender_edit.text(),
            'description': self.description_edit.text(),
            'profession': self.profession_edit.text(),
            'skills': self.skills_edit.text(),
            'company': self.company_edit.text(),
            'experience': self.experience_edit.text(),
            'linkedin': self.linkedin_edit.text(),
            'gitlab': self.gitlab_edit.text(),
            'instagram': self.instagram_edit.text(),
            'facebook': self.facebook_edit.text(),
            
        }
        doc_ref = db.collection('users').document(Active.activeUser)

        # Update the document with the new values
        doc_ref.update(new_values)

        print("Data updated successfully!")
        self.accept()  # Close the dialog after saving changes

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QLineEdit, QPushButton

class ChangePasswordDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)
        self.setWindowTitle("Change Password")

        layout = QVBoxLayout()  # Create a QVBoxLayout to hold the widgets

        self.password_label = QLabel("New Password:")
        self.password_label.setStyleSheet("font-size:14px;")
        self.password_edit = QLineEdit()
        self.password_edit.setStyleSheet("font-size:14px;")
        layout.addWidget(self.password_label)
        layout.addWidget(self.password_edit)

        self.save_button = QPushButton("Save")
        self.save_button.clicked.connect(self.save_password)
        self.save_button.setStyleSheet("background-color: #7D3C98; color:white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px;")
        layout.addWidget(self.save_button)

        back_button = QPushButton("Back")
        back_button.clicked.connect(self.exit)
        back_button.setStyleSheet("background-color: #7D3C98; color:white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px;")
        layout.addWidget(back_button)

        self.setLayout(layout)  # Set the layout to the dialog

    def exit(self):
        self.reject()

    def save_password(self):
        new_password = self.password_edit.text()
        if new_password:
            user_ref = db.collection('users').document(Active.activeUser)
            user_ref.update({'password': new_password})
            self.accept()  # Close the dialog if the password is saved successfully

    @staticmethod
    def get_new_password(parent=None):
        dialog = ChangePasswordDialog(parent)
        result = dialog.exec_()
        return result == QDialog.Accepted

class ProfilePage(QWidget):
    def __init__(self):
        super().__init__()
        self.c2w_init_ui()
    
    def applyCircularMask(self,pixmap,size,width,height):
        
        mask=QPixmap(size)
        mask.fill(QtCore.Qt.GlobalColor.transparent)

        painter=QPainter(mask)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)
        path=QPainterPath()
        path.addEllipse(0,0,width,height)
        painter.setClipPath(path)
        painter.fillPath(path,QColor(QtCore.Qt.GlobalColor.white))
        painter.end()
        masked=QPixmap(size)
        masked.fill(QtCore.Qt.GlobalColor.transparent)
        painter.begin(masked)
        painter.setClipPath(path)
        painter.drawPixmap(0,0,pixmap)
        painter.end()
        return masked

        
    def c2w_init_ui(self):
        doc_ref = db.collection('users').document(Active.activeUser)
        data = doc_ref.get() 
        print("I am reached in profilePage init UI")
        self.setFixedSize(1000,700)
        self.setWindowTitle('User Info')
        self.setGeometry(20, 40, 1000, 700)
        header_label = QLabel('My Profile')
        header_label.setAlignment(Qt.AlignmentFlag.AlignLeft)
        header_label.setStyleSheet('color: #333;margin:10px;padding: 20px; font-size: 44px; max-height:50px;')

        self.username_edit = QLabel(data.get('username'))
        self.username_edit.setStyleSheet("font-size: 18px")
        self.username_edit.setFixedWidth(300)
        self.fullname_edit = QLabel(data.get('fullname'))
        self.fullname_edit.setStyleSheet("font-size: 18px")
        self.fullname_edit.setFixedWidth(300)
        self.email_edit = QLabel(data.get('email'))
        self.email_edit.setStyleSheet("font-size: 18px")
        self.email_edit.setFixedWidth(300)
        self.pwd_edit = QLabel(data.get('password'))
        self.pwd_edit.setStyleSheet("font-size: 18px")
        self.pwd_edit.setFixedWidth(300)
        self.mobno_edit = QLabel(data.get('mobile'))
        self.mobno_edit.setStyleSheet("font-size: 18px")
        self.mobno_edit.setFixedWidth(300)
        self.profession_edit = QLabel(data.get('profession'))
        self.profession_edit.setStyleSheet("font-size: 18px")
        self.profession_edit.setFixedWidth(500)
        self.skills_edit = QLabel(data.get('skills'))
        self.skills_edit.setStyleSheet("font-size: 18px")
        self.skills_edit.setFixedWidth(500)
        self.username_label=QLabel("Username : ")
        self.username_label.setStyleSheet("font-size: 24px")
        self.fullname_label=QLabel("Full Name : ")
        self.fullname_label.setStyleSheet("font-size: 24px")
        self.email_label=QLabel("Email Id : ")
        self.email_label.setStyleSheet("font-size: 24px")
        self.mobno_label=QLabel("Mobile no : ")
        self.mobno_label.setStyleSheet("font-size: 24px")
        self.pwd_label=QLabel("Password : ")
        self.pwd_label.setStyleSheet("font-size: 24px")
        self.profession_label=QLabel("Profession : ")
        self.profession_label.setStyleSheet("font-size: 24px")
        
        user_edit_label=QLabel()
        img=data.get('profile_img')
        if img==None:
            if(data.get('gender')=='Female'):
                img="assets/profile_lady.png"
            else:
                img="assets/profile_men.png"
        user_edit_img = QPixmap(img)
        scaled_user_edit_img = user_edit_img.scaled(20, 20, Qt.AspectRatioMode.KeepAspectRatio)  # Adjust the width and height as needed
        user_edit_label.setPixmap(scaled_user_edit_img)
             

        self.change_pwd=QPushButton("Change Password")
        self.change_pwd.setMaximumWidth(160)
        self.change_pwd.setMaximumHeight(70)
        self.change_pwd.clicked.connect(self.changePwd)
        self.change_pwd.setStyleSheet("margin: 1px;background-color: #7D3C98; color: white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px; ")

        self.skills_label=QLabel("Skills : ")
        self.skills_label.setStyleSheet("font-size: 24px")
        self.back=QPushButton("Back")
        self.back.setMaximumWidth(120)
        self.back.setMaximumHeight(100)
        self.back.setStyleSheet("margin: 20px;background-color: #7D3C98; color:white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px; box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);")
        self.back.clicked.connect(self.exit_page)
        

        form_layout = QFormLayout()

        form_layout.addRow(self.username_label, self.username_edit)
        form_layout.addRow(self.fullname_label, self.fullname_edit)
        form_layout.addRow(self.email_label, self.email_edit)
        form_layout.addRow(self.mobno_label, self.mobno_edit)
        form_layout.addRow(self.pwd_label, self.pwd_edit)
        form_layout.addRow(QLabel(""),self.change_pwd)
        form_layout.addRow(self.profession_label, self.profession_edit)
        form_layout.addRow(self.skills_label, self.skills_edit)
        form_layout.addRow(QLabel(""))
        form_layout.setSpacing(20)
        form_layout.setFormAlignment(Qt.AlignmentFlag.AlignTop)
        form_layout.setContentsMargins(20,0,0,0)
        button_layout = QVBoxLayout()

        left_layout = QVBoxLayout()
        left_layout.addWidget(header_label)
        left_layout.addLayout(form_layout)
        left_layout.addStretch()  # Add stretch to push the form to the top
        left_layout.addLayout(button_layout)
        left_layout.addWidget(self.back,alignment=Qt.AlignmentFlag.AlignRight)

      
        newWidget = QWidget()
        self.image_label = QLabel(newWidget)
        user_img = QPixmap(img)
        scaled_pixmap = user_img.scaled(200, 200, Qt.AspectRatioMode.KeepAspectRatio)  # Adjust the width and height as needed
        
        self.image_label.setPixmap(scaled_pixmap)
        self.image_label.setMargin(40)
        edit_profile_picture_btn=QPushButton("Edit Profile Photo")
        edit_profile_picture_btn.clicked.connect(self.select_image)
        edit_profile_info_btn=QPushButton("Edit Personal Information")
        edit_profile_info_btn.setFixedWidth(220)
        edit_profile_info_btn.clicked.connect(self.open_edit_profile_dialog)
        edit_profile_picture_btn.setFixedWidth(220)
        newWidget.setStyleSheet("background-color: black ;border-radius:6px;")
        right_layout=QVBoxLayout(newWidget)
        right_layout.addWidget(self.image_label,alignment=Qt.AlignmentFlag.AlignHCenter)
        right_layout.addWidget(edit_profile_picture_btn,alignment=Qt.AlignmentFlag.AlignJustify)
        right_layout.addWidget(edit_profile_info_btn,alignment=Qt.AlignmentFlag.AlignJustify)
        edit_profile_picture_btn.setStyleSheet("margin: 1px;background-color:#7D3C98; color: white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px; box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);")
        edit_profile_info_btn.setStyleSheet("margin: 1px;background-color:#7D3C98; color: white; border: 2px solid #7D3C98; border-radius: 6px; padding: 5px 10px; font-size: 16px;")
        right_layout.setSpacing(10)
        right_layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        splitter = QSplitter(Qt.Orientation.Horizontal)
        splitter.addWidget(QWidget())
        splitter.addWidget(QWidget())
        splitter.setSizes([self.width() // 3, self.width()*2// 3])
        splitter.setMidLineWidth(60)
        splitter.widget(1).setLayout(left_layout)
        splitter.widget(1).setStyleSheet("background-color:white;border-radius:6px")
        splitter.widget(0).setLayout(QVBoxLayout())
        splitter.widget(0).layout().addWidget(newWidget)
        splitter.setFixedWidth(1000)
        main_layout = QVBoxLayout()
        main_layout.addWidget(splitter)
        self.setLayout(main_layout)
        print("I am reached in profilePage init UI")

    def open_edit_profile_dialog(self):
        doc_ref = db.collection('users').document(Active.activeUser)
        try:
            data = doc_ref.get().to_dict()
            current_values = {
            'username': data.get('username'),
            'email': data.get('email'),
            'password': data.get('password'),
            'fullname': data.get('fullname'),
            'mobile': data.get('mobile'),
            'profile_img': data.get('profile_img'),
            'gender': data.get('gender'),
            'description': data.get('description'),
            'profession': data.get('profession'),
            'skills': data.get('skills'),
            'company': data.get('company'),
            'experience': data.get('experience'),
            'linkedin': data.get('linkedin'),
            'gitlab': data.get('gitlab'),
            'instagram': data.get('instagram'),
            'facebook': data.get('facebook'),
            'no_star': data.get('no_star'),
            'gain_star': data.get('gain_star')
            }
            dialog = EditProfileDialog(current_values, self)
            if dialog.exec_() == QDialog.Accepted:
                print("Changes saved")
        except Exception as e:
            print("Error:", e)
        
    def changePwd(self):
        # Open the change password dialog
        if ChangePasswordDialog.get_new_password(self):
            print("Password changed successfully")
        else:
            print("Password change canceled or failed")


    def exit_page(self):
        self.close()
    
    def select_image(self):
        print('I am in selecting image')
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","Image Files (*.jpg *.png *.jpeg)", options=options)
        if fileName:
            self.pixmap = QPixmap(fileName)
            self.circular_pixmap=self.applyCircularMask(self.pixmap,self.pixmap.size(),self.pixmap.width(),self.pixmap.height())
            scaled_pixmap = self.circular_pixmap.scaled(200, 200, Qt.AspectRatioMode.KeepAspectRatio)
            self.image_label.setPixmap(scaled_pixmap)
            # Get the current directory
            current_dir = os.path.dirname(os.path.abspath(__file__))
        
            # Convert absolute path to relative path
            relative_path = os.path.relpath(fileName, current_dir)
            doc_ref = db.collection('users').document(Active.activeUser)
            doc_ref.update({'profile_img':relative_path[3:]})

if __name__ == "__main__":
    try:
        app = QApplication(sys.argv)
        ex = ProfilePage()
        ex.showFullScreen()
        #ex.setWindowIcon(QtGui.QIcon('assets/img1.jpg'))
        ex.show()
        sys.exit(app.exec_())
    except Exception as e:
        print(f"An error occurred: {e}")
