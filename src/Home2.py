import sys
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtCore import QSize, Qt, pyqtSignal
from PyQt5.QtGui import QIcon, QPainter, QPainterPath
from PyQt5.QtWidgets import QApplication, QBoxLayout, QLabel, QLineEdit, QPushButton, QVBoxLayout, QHBoxLayout, QFormLayout, QFileDialog, QRadioButton, QWidget, QMessageBox, QSplitter
from PyQt5.QtGui import QPixmap
from favorites import FavoritePage
# from favorites import FavoritePage, PersonItem

from profile_1 import ProfilePage
from sign_up import SignUp
from PyQt5.QtCore import Qt
from PyQt5.QtCore import Qt
from integration import Retrive,db,Active

from PyQt5.QtGui import QPixmap, QColor
from PyQt5.QtWidgets import (QGraphicsDropShadowEffect, QHBoxLayout, QVBoxLayout, QLabel,
                             QPushButton, QLineEdit, QMainWindow, QApplication, QWidget,
                             QFrame, QScrollArea,QDialog)


from about_us import AboutUsPage
from contact_us import ContactUsPage
from user_info import UserInfo

# Import OAuth libraries for Google and Facebook
# Install using pip install oauthlib requests requests_oauthlib
#from requests_oauthlib import OAuth2Session
#from oauthlib.oauth2 import BackendApplicationClient

# OAuth configuration for Google
google_client_id = 'YOUR_GOOGLE_CLIENT_ID'
google_client_secret = 'YOUR_GOOGLE_CLIENT_SECRET'
google_redirect_uri = 'http://localhost:8080/callback'
google_auth_url = 'https://accounts.google.com/o/oauth2/auth'
google_token_url = 'https://accounts.google.com/o/oauth2/token'
google_scope = ['openid', 'email', 'profile']

# OAuth configuration for Facebook
facebook_client_id = '285685197692803'
facebook_client_secret = '46e33ee354ac71a6f4241a77549fffec'
facebook_redirect_uri = 'http://localhost:8080/callback'
facebook_auth_url = 'https://www.facebook.com/v12.0/dialog/oauth'
facebook_token_url = 'https://graph.facebook.com/v12.0/oauth/access_token'
facebook_scope = ['7757902185','Mohit_Badgujar']



clickedUname=None


from PyQt5.QtWidgets import (QDialog, QVBoxLayout, QHBoxLayout, QLabel, QFrame, QWidget, QScrollArea)
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt

class ProfilePopup(QDialog):
    def __init__(self, user_data_list):
        super().__init__()

        self.setWindowTitle('User Profiles')
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)
        if Active.isLoggedIn:
            self.setGeometry(235, 100, 432, 400)
        else:
            self.setGeometry(592, 102, 439, 400)

        layout = QVBoxLayout()

        for user_data in user_data_list:
            img = user_data.get('profile_img')
            if img is None:
                img = "assets/profile/default_lady.jpg" if user_data.get('gender') == 'Female' else "assets/profile/default_men.jpg"

            profile_img_label = QLabel()
            pixmap = QPixmap(img)
            scaled_pixmap = pixmap.scaled(80, 80, Qt.AspectRatioMode.KeepAspectRatio)
            profile_img_label.setPixmap(scaled_pixmap)

            fullname_label = QLabel('Full Name: ' + user_data.get('fullname'))
            username_label = QLabel('Username: ' + user_data.get('username'))
            profession_label = QLabel('Profession: ' + user_data.get('profession'))

            fullname_label.setStyleSheet("color:white;font-size:14px;")
            username_label.setStyleSheet("color:white;font-size:14px;")
            profession_label.setStyleSheet("color:white;font-size:14px;")

            row_layout = QHBoxLayout()
            row_layout.setContentsMargins(0, 0, 0, 0)
            row_layout.setSpacing(10)
            row_wid = QWidget()
            row_wid.setLayout(row_layout)
            row_wid.setFixedSize(370, 103)  # Set width and height

            profile_img_label.setFixedSize(80, 80)  # Adjust profile image size

            info_layout = QVBoxLayout()
            info_layout.addWidget(fullname_label)
            info_layout.addWidget(username_label)
            info_layout.addWidget(profession_label)

            row_frame = QFrame()
            row_frame.setLayout(info_layout)
            row_frame.setStyleSheet("background-color:#7D3C98; border-radius: 15px; padding: 10px; margin: 5px;")

            row_layout.addWidget(profile_img_label)
            row_layout.addWidget(row_frame)
            layout.addWidget(row_wid)

        new_widget = QWidget()  # Create a new widget to hold the layout
        new_widget.setLayout(layout)

        # Create a scroll area and set its content to the new widget
        scroll_area = QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(new_widget)
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)

        # Set the size of the scroll area
        scroll_area.setFixedSize(443, 300)
        scroll_area.setFrameStyle(QFrame.Shape.NoFrame)
        scroll_area.setStyleSheet("background-color:white")
        main_layout = QVBoxLayout()
        main_layout.addWidget(scroll_area)

        self.setLayout(main_layout)


class ClickableWidget(QWidget):
    clicked = pyqtSignal()  # Signal to emit when clicked

    def __init__(self,parent=None):
        super().__init__(parent)
      
        self.setMouseTracking(True)  # Enable mouse tracking if needed

    def mousePressEvent(self, event):
        
        # Emit the clicked signal
        self.clicked.emit()
        # Open the new window

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel, QFrame
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt

class SearchResultWindow(QWidget):
    def __init__(self, user_data_list):
        super().__init__()
        self.user_data_list = user_data_list
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)
        self.setStyleSheet("background-color: none;")
        self.init_ui()

    def init_ui(self):
        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        print("Search result window")
        for user_data in self.user_data_list:
            # Assuming profile_img is retrieved correctly from the database
            profile_img = user_data.get('profile_img')
            fullname = user_data.get('fullname')
            username = user_data.get('username')
            profession = user_data.get('profession')
            
            if profile_img is None:
                profile_img = "assets/profile/default_lady.jpg" if user_data.get('gender') == 'Female' else "assets/profile/default_men.jpg"
            
            profile_img_label = QLabel()
            pixmap = QPixmap(profile_img)
            scaled_pixmap = pixmap.scaled(100, 100, Qt.AspectRatioMode.KeepAspectRatio)
            profile_img_label.setPixmap(scaled_pixmap)
            print(profile_img)
            fullname_label = QLabel(fullname)
            username_label = QLabel(username)
            profession_label = QLabel(profession)
            
            print(fullname)
            print(username)
            print(profession)
            
            card_layout = QHBoxLayout()
            card_layout.setContentsMargins(0, 0, 0, 0)
            card_layout.setSpacing(0)
            
            card_frame = QFrame()
            card_frame.setFixedWidth(500)
            card_frame.setStyleSheet("QFrame { background-color: #7D3C98; border-radius:15px; }")
            card_frame.setLayout(card_layout)

            card_layout.addWidget(profile_img_label)
            
            info_layout = QVBoxLayout()
            info_layout.addWidget(fullname_label)
            info_layout.addWidget(username_label)
            info_layout.addWidget(profession_label)
            
            card_layout.addLayout(info_layout)
            layout.addWidget(card_frame)
       
        self.setLayout(layout)
        print("search list ui completed")

class SearchBar(QMainWindow):
    displayName=""
    def __init__(self):
        print("I have started building search bar window")
        super().__init__()
        self.initUI()
        print("I have completed searchbar window")

    def initUI(self):
        self.setWindowTitle('Home')
        self.setStyleSheet("background-color: #ffffff")
       

        # Main widget
        main_widget = QWidget()
        main_layout = QVBoxLayout()
        main_widget.setLayout(main_layout)
        

        # Scroll area
        scroll_area = QScrollArea()
        scroll_widget = QWidget()
        scroll_layout = QVBoxLayout()
        #scroll_area.setFixedSize(1300,600)
        scroll_widget.setLayout(scroll_layout)
        scroll_widget.setStyleSheet("background-color:black")
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(scroll_widget)

        # Set vertical scroll policy
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        appbar_widget=QWidget()
        # Appbar
        appbar = QHBoxLayout()  
        appbar.setAlignment(Qt.AlignmentFlag.AlignTop)

        logo_label = QLabel()
        pixmap = QPixmap('assets/Logo/horizontal logo.png')
        scaled_pixmap = pixmap.scaled(200, 250, Qt.AspectRatioMode.KeepAspectRatio)
        logo_label.setPixmap(scaled_pixmap)
        appbar.addWidget(logo_label)

        self.search_input = QLineEdit()
        self.search_input.setStyleSheet("border-radius: 15px;background-color:white;padding: 10px;border: 2px solid #0c0d0d;")
        self.search_input.setPlaceholderText("Enter your text here...")
        self.search_input.setFixedSize(490,40)
        #self.search_input.setContentsMargins(30,5,20,0)
        appbar.addWidget(self.search_input)

        self.search_button = QPushButton('Search')
        self.search_button.setStyleSheet("color: white;font-size:20; background-color: #7D3C98; font-weight: bold; border-radius: 15px;padding: 5px; border: 2px solid #0c0d0d;")
        self.search_button.setFixedSize(100,45)
        #self.search_button.setContentsMargins(460,5,500,0)
        self.search_button.clicked.connect(self.search_list)
        appbar.addWidget(self.search_button,Qt.AlignmentFlag.AlignLeft)

        appbar.addStretch(1)

        icon_wid=QHBoxLayout()

        favorites_button = QPushButton()
        favorites_button.setStyleSheet("margin:0px;color: white; font-weight: bold; border-radius: 25px;padding: 2px;")
        icon1 = QIcon("assets/Icons/fav_hover.png")
        favorites_button.setIcon(icon1)
        # Set the icon size for the button
        favorites_button.setIconSize(icon1.actualSize(QSize(50,50)))
        
        favorites_button.clicked.connect(self.openFav)
        
        icon_wid.addWidget(favorites_button)
        
        notification_button = QPushButton()
        notification_button.setStyleSheet("margin:0px;color: white; font-weight: bold; border-radius: 25px;padding: 2px;")
        icon2 = QIcon("assets/Icons/bell.png")
        notification_button.setIcon(icon2)
        notification_button.setIconSize(icon2.actualSize(QSize(50,50)))
       
        icon_wid.addWidget(notification_button,Qt.AlignmentFlag.AlignRight)
        
        about_button = QPushButton()
        about_button.setStyleSheet("color: white; font-weight: bold; border-radius: 25px;padding: 2px;")
        icon3 = QIcon("assets/Icons/about_us.png")
        about_button.setIcon(icon3)
        about_button.setIconSize(icon3.actualSize(QSize(50,50)))
        about_button.clicked.connect(self.aboutPage)
        icon_wid.addWidget(about_button,Qt.AlignmentFlag.AlignRight)

        contact_button = QPushButton()
        contact_button.setStyleSheet("color: white; font-weight: bold; border-radius: 25px;padding: 2px;")
        icon4 = QIcon("assets/Icons/contact.png")
        contact_button.setIcon(icon4)
        contact_button.setIconSize(icon4.actualSize(QSize(50,50)))
        contact_button.clicked.connect(self.contactPage)
        icon_wid.addWidget(contact_button,Qt.AlignmentFlag.AlignRight)

        ref = db.collection('users').document(Active.activeUser).get()
        img_dict = ref.to_dict()
        profile_img = img_dict.get('profile_img')

        profile_button = QPushButton()
        profile_button.setStyleSheet("padding:2px;")
        icon6=QIcon(profile_img)
        profile_button.setIcon(icon6)
        profile_button.setIconSize(icon6.actualSize(QSize(60,60)))
        profile_button.clicked.connect(self.openEditProfile)
        icon_wid.addWidget(profile_button,Qt.AlignmentFlag.AlignRight)

        icon_wid.addWidget(profile_button)
        
        log_out_button = QPushButton()
        log_out_button.setStyleSheet("padding: 2px;")
        icon5 = QIcon("assets/Icons/log_out.png")
        log_out_button.setIcon(icon5)
        log_out_button.setIconSize(icon5.actualSize(QSize(50,50)))
        icon_wid.addWidget(log_out_button)
        log_out_button.clicked.connect(self.logOut)

        icon_wid.setSpacing(6)
        appbar.addLayout(icon_wid)
        appbar_widget.setLayout(appbar)
        #appbar_widget.setFixedWidth(1300)
        appbar_widget.setFixedHeight(150)
        appbar_widget.setStyleSheet("background-color:black;border-radius:20px")
      
        main_layout.addWidget(appbar_widget)
        main_layout.addWidget(scroll_area)

        # Add ShadowedImageWidget
        shadowed_image_widget = ShadowedImageWidget('assets/images/home_400x280.jpg')
        shadowed_image_widget.setFixedHeight(450)
        shadowed_image_widget.setStyleSheet("border-radius: 20px;background-color: white;")
        scroll_layout.addWidget(shadowed_image_widget)

        # Add first label
        label1 = QLabel(f"Welcome  {SearchBar.displayName} ,")
        label1.setStyleSheet("color:black; font-size: 40px; font-weight: bold;padding: 0; background-color: None; border-radius: 20px; border-style: solid;padding: 13px")
        label1.setAlignment(Qt.AlignmentFlag.AlignRight)
        shadowed_image_widget.add_label(label1)

        # Add second label
        label2 = QLabel('Find your perfect partner based on interests, \n\t skills and location.')
        label2.setStyleSheet("color:black; font-size: 25px; font-weight: regular;background-color: None; border-radius: 20px; padding: 0; border-style: solid;padding: 13px")
        label2.setAlignment(Qt.AlignmentFlag.AlignRight)
        shadowed_image_widget.add_label(label2)

        # Frontend Developer
        dev1_label = QLabel("FullStack Developer")
        dev1_label.setStyleSheet("color: black; font-size: 30px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: white")
        dev1_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev1_label)

        # Add first row
        first_row_container = QWidget()
        first_row_layout = QHBoxLayout()
        first_row_container.setLayout(first_row_layout)
  

        try:
            # Query documents where profession is "FullStack Developer"
            query1 = db.collection('users').where('profession', '==', 'FullStack Developer').stream()

            # Iterate over each document
            for user_doc in query1:
                # Extract data from document
                user_data = user_doc.to_dict()
                image_layout = QVBoxLayout()
                if(user_data.get('username')!=clickedUname):
                    # Add the image
                    label = QLabel()
                    img = user_data.get('profile_img')
                    if img is None:
                        img = "assets/profile/default_lady.jpg" if user_data.get('gender') == 'Female' else "assets/profile/default_men.jpg"
                    pixmap = QPixmap(img)
                    label.setPixmap(pixmap)
                    label.setStyleSheet("color: black; padding: 10px;border-radius: 5px;")
                    label.setScaledContents(True)
                    label.setFixedSize(250, 250)
                    image_layout.addWidget(label)

                    fname = user_data.get('fullname')
                    desc = user_data.get('profession')
                    # Add the Names
                    names_label = QLabel(f"{fname}<br>{desc}")
                    names_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
                    names_label.setStyleSheet("background-color: #7D3C98; color: white; padding: 5px; border-radius: 5px;font-weight: bold;font-size: 15px")
                    names_label.setFixedSize(250, 60)
                    image_layout.addWidget(names_label)

                    clickable_widget = ClickableWidget()  # Create clickable widget instance
                    clickable_widget.setLayout(image_layout)
                    # Assuming openInfo accepts a string parameter
                    clickable_widget.clicked.connect(lambda username=user_data.get('username'): self.openInfo(username))

                    first_row_layout.addWidget(clickable_widget)

        except Exception as e:
            print("Error retrieving users:", e)

        first_row_scroll_area = QScrollArea()
        first_row_scroll_area.setWidgetResizable(True)
        first_row_scroll_area.setFixedHeight(400)
        first_row_scroll_area.setWidget(first_row_container)
        first_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        first_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_layout.addWidget(first_row_scroll_area) 
        # Backend Developer
        dev2_label = QLabel("Backend Developer")
        dev2_label.setStyleSheet("color: black;background-color:white; font-size: 30px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; ")
        dev2_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev2_label)
    
        # Add first row
        second_row_container = QWidget()
        second_row_layout = QHBoxLayout()
        second_row_container.setLayout(second_row_layout)
        
        try:
            # Query documents where profession is "FullStack Developer"
            query2 = db.collection('users').where('profession', 'in', ['Backend Developer','FullStack Developer']).stream()

            # Iterate over each document
            for user_doc in query2:
                # Extract data from document
                user_data2 = user_doc.to_dict()
                image_layout2 = QVBoxLayout()

                # Add the image
                label2 = QLabel()
                img2=user_data2.get('profile_img')    
                if(img2 == None):
                    if(user_data2.get('gender')=='Female'):
                        img2="assets/profile/default_lady.jpg"
                    else:
                        img2="assets/profile/default_men.jpg"
                pixmap2 = QPixmap(img2)
                print(img2)
                label2.setPixmap(pixmap2)
                label2.setStyleSheet(" color: black; padding: 10px;border-radius: 5px;")
                label2.setScaledContents(True)  
                label2.setFixedSize(250, 250)  
                image_layout2.addWidget(label2)
                fname2=user_data2.get('fullname')
                desc2=user_data2.get('profession')
                # Add the Names
                names_label2 = QLabel(fname2+  "<br>"  + desc2)
                print(user_data2.get('fullname'))
                print(user_data2.get('profession'))
                print("==========")
                names_label2.setAlignment(Qt.AlignmentFlag.AlignCenter)
                names_label2.setStyleSheet("color: white; background-color: #7D3C98;padding: 5px; border-radius: 5px;font-weight: bold;font-size: 15px")
                names_label2.setFixedSize(250, 60)  
                image_layout2.addWidget(names_label2)
                '''            
                second_row_layout.addLayout(image_layout2)'''
                clickable_widget2 = ClickableWidget()  # Create clickable widget instance
                clickable_widget2.setLayout(image_layout2)
                # Assuming openInfo accepts a string parameter
                clickable_widget2.clicked.connect(lambda username=user_data2.get('username'): self.openInfo(username))

                second_row_layout.addWidget(clickable_widget2)

                
        except Exception as e:
            print("Error retrieving users:", e)


        second_row_scroll_area = QScrollArea()
        second_row_scroll_area.setWidgetResizable(True)
        second_row_scroll_area.setFixedHeight(400)
        second_row_scroll_area.setWidget(second_row_container)
        second_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)  # Add this line to set horizontal scrollbar policy
        second_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_layout.addWidget(second_row_scroll_area)
        
        # Backend Developer
        dev3_label = QLabel("UIUX Design")
        dev3_label.setStyleSheet("color: black; background-color:white;font-size: 20px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; ")
        dev3_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev3_label)
    
        # Add first row
        third_row_container = QWidget()
        third_row_layout = QHBoxLayout()
        third_row_container.setLayout(third_row_layout)
        
        try:
            # Query documents where profession is "FullStack Developer" or "Frontend Developer"
            query3 = db.collection('users').where('profession', 'in', ['Frontend Developer','FullStack Developer']).stream()
            
            # Iterate over each document
            for user_doc in query3:
                # Extract data from document
                user_data3 = user_doc.to_dict()
                
                image_layout3 = QVBoxLayout()

                # Add the image
                label3 = QLabel()
                img3 = user_data3.get('profile_img', '') # Default to empty string if profile_img is not present
                if img3==None:
                    if user_data3.get('gender') == 'Female':
                        img3 = "assets/profile/default_lady.jpg"
                    else:
                        img3 = "assets/profile/default_men.jpg"
                pixmap3 = QPixmap(img3)
                label3.setPixmap(pixmap3)
                label3.setStyleSheet("color: black; padding: 10px; border-radius: 5px;")
                label3.setScaledContents(True)
                label3.setFixedSize(250, 250)
                image_layout3.addWidget(label3)
        
                # Add the Names
                fullname3 = user_data3.get('fullname')
                profession3 = user_data3.get('profession')
                names_label3 = QLabel(f"{fullname3}<br>{profession3}")
                names_label3.setAlignment(Qt.AlignmentFlag.AlignCenter)
                names_label3.setStyleSheet("color: white; background-color: #7D3C98; padding: 5px; border-radius: 5px; font-weight: bold; font-size: 15px")
                names_label3.setFixedSize(250, 60)
                image_layout3.addWidget(names_label3)

                clickable_widget3 = ClickableWidget()  # Create clickable widget instance
                clickable_widget3.setLayout(image_layout3)
                # Assuming openInfo accepts a string parameter
                clickable_widget3.clicked.connect(lambda username=user_data3.get('username'): self.openInfo(username))

                
                third_row_layout.addWidget(clickable_widget3)

        except Exception as e:
            print("Error retrieving users:", e)

        third_row_scroll_area = QScrollArea()
        third_row_scroll_area.setWidgetResizable(True)
        third_row_scroll_area.setFixedHeight(400)
        third_row_scroll_area.setWidget(third_row_container)
        third_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)  # Add this line to set horizontal scrollbar policy
        third_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_layout.addWidget(third_row_scroll_area)

        copyright_label = QLabel("© Copyright 2024 Click&Collab. All rights reserved.")
        copyright_label.setStyleSheet("color: white; font-size: 30px;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: black;border-radius : 10px")
        copyright_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(copyright_label)
 
        self.setCentralWidget(main_widget)

    def logOut(self):
        Active.isLoggedIn=False
        reOpen=HomePage()
        reOpen.showFullScreen()
        reOpen.show()

    def openEditProfile(self):
        self.openEditProfile=ProfilePage()
        self.openEditProfile.showFullScreen()
        self.openEditProfile.show()

    def applyCircularMask(self, pixmap, size, width, height):
        mask = QPixmap(size)
        mask.fill(QtCore.Qt.GlobalColor.transparent)

        painter = QPainter(mask)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)
        path = QPainterPath()
        path.addEllipse(0, 0, width, height)
        painter.setClipPath(path)
        painter.fillPath(path, QtGui.QColor(QtCore.Qt.GlobalColor.white))
        painter.end()
        masked = QPixmap(size)
        masked.fill(QtCore.Qt.GlobalColor.transparent)
        painter.begin(masked)
        painter.setClipPath(path)
        painter.drawPixmap(0, 0, pixmap)
        painter.end()
        return masked
    
    def openFav(self):
        if(Active.isLoggedIn):
            self.open_fav_window=FavoritePage()
            self.open_fav_window.showFullScreen()
            self.open_fav_window.show()
        else:
            QMessageBox.critical(self, "Error", "Please Login First")
        

    def aboutPage(self):
        self.open_about=AboutUsPage()
        self.open_about.showFullScreen()
        self.open_about.show()

    def contactPage(self):
        self.open_contact=ContactUsPage()
        self.open_contact.showFullScreen()
        self.open_contact.show()
    
    def search_list(self):
        try:
            text = self.search_input.text()
            fullname_query = db.collection('users').where('fullname', '==', text).stream()
            profession_query = db.collection('users').where('profession', '==', text).stream()

            users = []

            for doc in fullname_query:
                users.append(doc)

            for doc in profession_query:
                users.append(doc)

            user_data_list = []

            for doc in users:
                user_data = doc.to_dict()
                user_data_list.append(user_data)

            profile_popup = ProfilePopup(user_data_list)
            profile_popup.exec_()

        except Exception as e:
            print("An error occurred:", e)

        # Define the openInfo function
    def openInfo(self,username):
        print("Open info for user:", username)
        self.open_profile = UserInfo(username)
        self.open_profile.showFullScreen()
        self.open_profile.show()

    def search(self):
        search_query = self.search_input.text()
        print("Search query:", search_query)
    '''
    def show_menu_window(self):
        menu_window = NavigationBar()
        menu_window.show()
        menu_window.exec_()
'''

class ShadowedImageWidget(QLabel):
    def __init__(self, image_path, parent=None):
        super().__init__(parent)
        self.set_image_with_shadow(image_path)
        self.labels_layout = QVBoxLayout()
        self.labels_layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.setLayout(self.labels_layout)

    def set_image_with_shadow(self, image_path):
        pixmap = QPixmap(image_path)
        self.setPixmap(pixmap)
        shadow_effect = QGraphicsDropShadowEffect()
        shadow_effect.setBlurRadius(40)
        shadow_effect.setColor(QColor(0, 0, 0, 200))
        shadow_effect.setOffset(4, 4)

        self.setGraphicsEffect(shadow_effect)

    def add_label(self, label):
        self.labels_layout.addWidget(label)

#########-------------Home Page before login----------------

class HomePage(QMainWindow):

    def __init__(self):
        print("opening Home Page")
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Home')
        self.setStyleSheet("background-color: #ffffff")
        #self.setGeometry(5, 40, 1400, 700)

        # Main widget
        main_widget = QWidget()
        main_layout = QVBoxLayout()
        main_widget.setLayout(main_layout)
        

        # Scroll area
        scroll_area = QScrollArea()
        scroll_widget = QWidget()
        scroll_widget.setStyleSheet("background-color:black")
        scroll_layout = QVBoxLayout()
        #scroll_area.setFixedSize(1300,700)
        scroll_widget.setLayout(scroll_layout)
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(scroll_widget)

        # Set vertical scroll policy
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        appbar_widget=QWidget()
        # Appbar
        appbar = QHBoxLayout()  
        appbar.setAlignment(Qt.AlignmentFlag.AlignTop)

        logo_label = QLabel()
        pixmap = QPixmap('assets/Logo/horizontal logo.png')
        scaled_pixmap = pixmap.scaled(500, 270, Qt.AspectRatioMode.KeepAspectRatio)
        logo_label.setPixmap(scaled_pixmap)
        appbar.addWidget(logo_label)

        self.search_input = QLineEdit()
        self.search_input.setStyleSheet("background-color:white;border-radius: 15px;padding: 10px;border: 2px solid #0c0d0d;")
        self.search_input.setPlaceholderText("Enter your text here...")
        self.search_input.setFixedSize(450,50)
        appbar.addWidget(self.search_input)

        search_button = QPushButton('Search')
        search_button.setStyleSheet("color: white;font-size:18;background-color: #7D3C98; font-weight: bold; border-radius: 15px;padding: 5px; border: 2px solid #0c0d0d;")
        search_button.setFixedSize(150,50)
        search_button.clicked.connect(self.search_list)

        appbar.addWidget(search_button)

        login_button = QPushButton('Login / SignUp')
        login_button.setStyleSheet("color: white;background-color: #7D3C98; font-weight: bold; border-radius: 15px;padding: 10px;border: 2px solid #0c0d0d;font-size:18")
        login_button.setFixedSize(150,50)
        login_button.clicked.connect(self.open_login)
        appbar.addWidget(login_button)
        appbar.setAlignment(Qt.AlignmentFlag.AlignTop)
        appbar_widget.setFixedHeight(150)
        #appbar_widget.setFixedWidth(1300)
        appbar_widget.setStyleSheet("background-color:black;border-radius:20px;")
        appbar_widget.setLayout(appbar)
        main_layout.addWidget(appbar_widget)
        #main_layout.addLayout(appbar)
        main_layout.addWidget(scroll_area)

        # Add ShadowedImageWidget
        shadowed_image_widget = ShadowedImageWidget('assets/images/home_400x280.jpg')
        shadowed_image_widget.setFixedHeight(450)
        shadowed_image_widget.setStyleSheet("border-radius: 20px;background-color: #FCFCFC;")
        scroll_layout.addWidget(shadowed_image_widget)

        # Add first label
        label1 = QLabel('Together We Rise ')
        label1.setStyleSheet("color:black; font-size: 60px; font-weight: bold;padding: 0; background-color: None; border-radius: 20px; border-style: solid;padding: 13px")
        label1.setAlignment(Qt.AlignmentFlag.AlignRight)
        shadowed_image_widget.add_label(label1)

        # Add second label
        label2 = QLabel('Find your perfect partner based on interests, \n\t skills and location.')
        label2.setStyleSheet("color:black; font-size: 40px; font-weight: regular;background-color: None; border-radius: 20px; padding: 0; border-style: solid;padding: 13px")
        label2.setAlignment(Qt.AlignmentFlag.AlignRight)
        shadowed_image_widget.add_label(label2)

        # Frontend Developer
        dev1_label = QLabel("FullStack Developer")
        dev1_label.setStyleSheet("color: black; font-size: 40px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: white")
        dev1_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev1_label)
    
        # Add first row
        first_row_container = QWidget()
        
        first_row_layout = QHBoxLayout()
        first_row_container.setLayout(first_row_layout)
        

        try:
            # Query documents where profession is "FullStack Developer"
            query1 = db.collection('users').where('profession', '==', 'FullStack Developer').stream()

            # Iterate over each document
            for user_doc in query1:
                # Extract data from document
                user_data = user_doc.to_dict()
                image_layout1 = QVBoxLayout()

                # Add the image
                label = QLabel()
                img = user_data.get('profile_img')
                print(img)    
                if img is None:
                    if user_data.get('gender') == 'Female':
                        img = "assets/profile/default_lady.jpg"
                    else:
                        img = "assets/profile/default_men.jpg"
                pixmap = QPixmap(img)
                label.setPixmap(pixmap)
                label.setStyleSheet("color: black; padding: 10px;border-radius: 5px;")
                label.setScaledContents(True)  
                label.setFixedSize(250, 250)  
                image_layout1.addWidget(label)

                fname = user_data.get('fullname')
                desc = user_data.get('profession')

                # Add the Names
                names_label = QLabel(f"{fname}<br>{desc}")
                names_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
                names_label.setStyleSheet("background-color: #7D3C98; color: white; padding: 5px; border-radius: 5px;font-weight: bold;font-size: 20px")
                names_label.setFixedSize(250, 60)  
                image_layout1.addWidget(names_label)
        
                
                # Connect to the click event
                clickable_widget1 = ClickableWidget()  # Create clickable widget instance
                clickable_widget1.setLayout(image_layout1)
                clickable_widget1.clicked.connect(lambda username=user_data.get('username'): self.openInfo(username))

                # Add clickable widget to the first row layout
                first_row_layout.addWidget(clickable_widget1)

        except Exception as e:
            print("Error retrieving users:", e)

        first_row_scroll_area = QScrollArea()
        first_row_scroll_area.setWidgetResizable(True)
        first_row_scroll_area.setFixedHeight( 400)
        first_row_scroll_area.setWidget(first_row_container)
        first_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)  
        first_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_layout.addWidget(first_row_scroll_area)
        
        # Backend Developer
        dev2_label = QLabel("Backend Developer")
        dev2_label.setStyleSheet("color: black; font-size: 40px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: white")
        dev2_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev2_label)
    
        # Add first row
        second_row_container = QWidget()
        second_row_layout = QHBoxLayout()
        second_row_container.setLayout(second_row_layout)
        
        try:
            # Query documents where profession is "FullStack Developer"
            query2 = db.collection('users').where('profession', 'in', ['Backend Developer','FullStack Developer']).stream()

            
            # Iterate over each document
            for user_doc in query2:
                # Extract data from document
                user_data2 = user_doc.to_dict()
                image_layout2 = QVBoxLayout()

                # Add the image
                label2 = QLabel()
                img2=user_data2.get('profile_img')    
                if(img2 == None):
                    if(user_data2.get('gender')=='Female'):
                        img2="assets/profile/default_lady.jpg"
                    else:
                        img2="assets/profile/default_men.jpg"
                pixmap2 = QPixmap(img2)
                print(img2)
                label2.setPixmap(pixmap2)
                label2.setStyleSheet(" color: black; padding: 10px;border-radius: 5px;")
                label2.setScaledContents(True)  
                label2.setFixedSize(250, 250)  
                image_layout2.addWidget(label2)
                fname2=user_data2.get('fullname')
                desc2=user_data2.get('profession')
                # Add the Names
                names_label2 = QLabel(fname2+  "<br>"  + desc2)
                print(user_data2.get('fullname'))
                print(user_data2.get('profession'))
                print("==========")
                names_label2.setAlignment(Qt.AlignmentFlag.AlignCenter)
                names_label2.setStyleSheet("background-color: #7D3C98; color: white; padding: 5px; border-radius: 5px;font-weight: bold;font-size: 20px")
                names_label2.setFixedSize(250, 60)  
                image_layout2.addWidget(names_label2)
                clickable_widget2 = ClickableWidget()  # Create clickable widget instance
                clickable_widget2.setLayout(image_layout2)
                # Assuming openInfo accepts a string parameter
                clickable_widget2.clicked.connect(lambda username=user_data2.get('username'): self.openInfo(username))
            
                second_row_layout.addWidget(clickable_widget2)
                
        except Exception as e:
            print("Error retrieving users:", e)

        second_row_scroll_area = QScrollArea()
        second_row_scroll_area.setWidgetResizable(True)
        second_row_scroll_area.setFixedHeight(400)
        second_row_scroll_area.setWidget(second_row_container)
        second_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)  # Add this line to set horizontal scrollbar policy
        second_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_layout.addWidget(second_row_scroll_area)
        
        # Backend Developer
        dev3_label = QLabel("Frontend Developer")
        dev3_label.setStyleSheet("color: black; font-size: 40px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: white")
        dev3_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(dev3_label)
    
        # Add first row
        third_row_container = QWidget()
        third_row_layout = QHBoxLayout()
        third_row_container.setLayout(third_row_layout)
        
        try:
             # Query documents where profession is "FullStack Developer" or "Frontend Developer"
            query3 = db.collection('users').where('profession', 'in', ['Frontend Developer','FullStack Developer']).stream()
            
            # Iterate over each document
            for user_doc in query3:
                # Extract data from document
                user_data3 = user_doc.to_dict()
                
                image_layout3 = QVBoxLayout()

                # Add the image
                label3 = QLabel()
                img3 = user_data3.get('profile_img', '') # Default to empty string if profile_img is not present
                if not img3:
                    if user_data3.get('gender') == 'Female':
                        img3 = "assets/profile/default_lady.jpg"
                    else:
                        img3 = "assets/profile/default_men.jpg"
                pixmap3 = QPixmap(img3)
                label3.setPixmap(pixmap3)
                label3.setStyleSheet("color: black; padding: 10px; border-radius: 5px;")
                label3.setScaledContents(True)
                label3.setFixedSize(250,250)
                image_layout3.addWidget(label3)
        
                # Add the Names
                fullname3 = user_data3.get('fullname', '')
                profession3 = user_data3.get('profession', '')
                names_label3 = QLabel(f"{fullname3}<br>{profession3}")
                names_label3.setAlignment(Qt.AlignmentFlag.AlignCenter)
                names_label3.setStyleSheet("background-color: #7D3C98; color: white; padding: 5px; border-radius: 5px; font-weight: bold; font-size: 20px")
                names_label3.setFixedSize(250, 60)
                image_layout3.addWidget(names_label3)
                clickable_widget3 = ClickableWidget()  # Create clickable widget instance
                clickable_widget3.setLayout(image_layout3)
                # Assuming openInfo accepts a string parameter
                clickable_widget3.clicked.connect(lambda username=user_data3.get('username'): self.openInfo(username))

                third_row_layout.addWidget(clickable_widget3)

        except Exception as e:
            print("Error retrieving users:", e)

        # Create scroll area for the third row
        third_row_scroll_area = QScrollArea()
        third_row_scroll_area.setWidgetResizable(True)
        third_row_scroll_area.setFixedHeight(400)
        third_row_scroll_area.setWidget(third_row_container)
        third_row_scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        third_row_scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        scroll_layout.addWidget(third_row_scroll_area)

        copyright_label = QLabel("© Copyright 2024 Click&Collab. All rights reserved.")
        copyright_label.setStyleSheet("color: white; font-size: 25px; font-weight: bold;margin-top: 20px; margin-bottom: 20px; padding: 10px; background-color: black")
        copyright_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        scroll_layout.addWidget(copyright_label)

        self.setCentralWidget(main_widget)

    

    def open_login(self):
        self.login_page=LoginPage()
        self.login_page.showFullScreen()
        self.login_page.show()

    def openInfo(self,username):
        print("Open info for user:", username)
        self.open_profile = UserInfo(username)
        self.open_profile.showFullScreen()
        self.open_profile.show()


    def search_list(self):
        try:
            text = self.search_input.text()
            fullname_query = db.collection('users').where('fullname', '==', text).stream()
            profession_query = db.collection('users').where('profession', '==', text).stream()

            users = []

            for doc in fullname_query:
                users.append(doc)

            for doc in profession_query:
                users.append(doc)

            user_data_list = []

            for doc in users:
                user_data = doc.to_dict()
                user_data_list.append(user_data)

            profile_popup = ProfilePopup(user_data_list)
            profile_popup.exec_()

        except Exception as e:
            print("An error occurred:", e)

#####--------Login Page----------------#######

class LoginPage(QWidget):
    def __init__(self):
        super().__init__()
        self.c2w_init_ui()
        
    def c2w_init_ui(self):

        header_label = QLabel('Login')
        header_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        header_label.setStyleSheet('color: #333;padding: 10px;margin:50px; font-size: 40px; max-height:50px;text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.5);')

        self.c2w_username_edit = QLineEdit()
        self.c2w_username_edit.setFixedWidth(200)
        self.c2w_username_edit.setFixedHeight(28)
        self.c2w_username_edit.setStyleSheet("border:2px solid black")
        self.c2w_username_edit.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.c2w_password_edit = QLineEdit()
        self.c2w_password_edit.setFixedWidth(200)
        self.c2w_password_edit.setFixedHeight(28)
        self.c2w_password_edit.setStyleSheet("border:2px solid black")
        self.c2w_password_edit.setEchoMode(QLineEdit.Password)

        self.c2w_submit_button = QPushButton('Login')
        self.c2w_submit_button.setFixedWidth(100)
        self.c2w_submit_button.setStyleSheet("margin-left:0px;background-color: #7D3C98; color: white;border: 7px solid #7D3C98;font-size:16px; border-radius: 10px;")
        self.c2w_submit_button.clicked.connect(self.open_home_page)

        create_new_label = QLabel('Don\'t have an account? ''<a href="#">SignUp.</a>')
        create_new_label.setAlignment(Qt.AlignmentFlag.AlignTop)
        create_new_label.setOpenExternalLinks(False)
        create_new_label.setStyleSheet("color: blue;margin-right:0px; margin-left:0px;text-decoration: underline;font-size:16px")
        create_new_label.linkActivated.connect(self.c2w_open_sign_up_page)

        self.c2w_back_button = QPushButton('Back')
        self.c2w_back_button.setFixedWidth(100)
        self.c2w_back_button.setStyleSheet("margin-left:0px;background-color: #7D3C98; color: white;border: 7px solid #7D3C98;font-size:16px; border-radius: 10px; ")
        self.c2w_back_button.clicked.connect(self.c2w_back_form)

        other_login_label = QLabel("<b>----------Also login with----------</b>")
        other_login_label.setStyleSheet("font-size: 16px; color: #333;margin:5px")
        other_login_label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        google_login_button = QPushButton()
        google_login_button.setIcon(QtGui.QIcon("assets/Mohit/google.png"))
        google_login_button.setIconSize(QtCore.QSize(30, 30))
        google_login_button.setFixedSize(50,50)
        google_login_button.setStyleSheet("border-radius: 10px")
        google_login_button.clicked.connect(self.login_with_google)

        facebook_login_button = QPushButton()
        facebook_login_button.setIcon(QtGui.QIcon("assets/Mohit/fb.png"))
        facebook_login_button.setIconSize(QtCore.QSize(30, 30))
        facebook_login_button.setFixedSize(50,50)
        facebook_login_button.setStyleSheet("border-radius: 10px")
        facebook_login_button.clicked.connect(self.login_with_facebook)

        # Create layout for Google and Facebook buttons
        other_login_button_layout = QHBoxLayout()
        other_login_button_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)
        other_login_button_layout.addWidget(google_login_button)
        other_login_button_layout.addWidget(facebook_login_button)



        self.username_label = QLabel("Username : ")
        self.username_label.setStyleSheet("font-size: 24px;margin-left:100px")
        self.pwd_label = QLabel("Password : ")
        self.pwd_label.setStyleSheet("font-size: 24px")

        form_layout = QFormLayout()
        form_layout.addRow(self.username_label, self.c2w_username_edit)
        form_layout.addRow(self.pwd_label, self.c2w_password_edit)
        form_layout.addRow(QLabel(""), self.c2w_submit_button)
        form_layout.addRow(QLabel(""), create_new_label)
        form_layout.addRow(other_login_label)  # Add other login label
        form_layout.addRow(other_login_button_layout)
        form_layout.addRow(QLabel(""),self.c2w_back_button) # Add Google and Facebook buttons
        form_layout.setSpacing(30)
        form_layout.setFormAlignment(Qt.AlignmentFlag.AlignTop)
        form_layout.setLabelAlignment(Qt.AlignmentFlag.AlignRight)
        form_layout.setHorizontalSpacing(30)

        left_layout = QVBoxLayout()
        left_layout.addWidget(header_label)
        left_layout.addLayout(form_layout)
        left_layout.addWidget(create_new_label)
        
        newWidget = QWidget()
        image_label = QLabel(newWidget)
        pixmap = QPixmap("assets/Mohit/login.jpeg")
        scaled_pixmap = pixmap.scaled(450, 1000, Qt.AspectRatioMode.KeepAspectRatio)
        image_label.setPixmap(scaled_pixmap)
        image_layout = QVBoxLayout(newWidget)
        image_layout.setAlignment(Qt.AlignmentFlag.AlignCenter)
        image_layout.addWidget(image_label)

        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(QWidget())
        splitter.addWidget(QWidget())
        splitter.setSizes([self.width() // 2, self.width() // 2])
        splitter.widget(1).setLayout(left_layout)
        splitter.widget(0).setLayout(QVBoxLayout())
        splitter.widget(0).layout().addWidget(newWidget)
        splitter.widget(1).setStyleSheet("background-color:#FFFFFF;border-radius:6px;")
        splitter.widget(0).setStyleSheet("background-color:#000000;border-radius:6px;")

        main_layout = QVBoxLayout()
        main_layout.addWidget(splitter)
        self.setLayout(main_layout)

    def c2w_back_form(self):
        self.close()

    def c2w_submit_form(self):
        username = self.c2w_username_edit.text()
        password = self.c2w_password_edit.text()
        if not username or not password:
            QMessageBox.critical(self, "Error", "Please fill in all required fields.")
            return False
        else:
            if(Retrive.retrieve_user_data(username)):
                query = db.collection('users').where('username', '==', username).limit(1).stream()

                for doc in query:
                    user_data = doc.to_dict()
                    fullname = user_data.get('fullname')
                    SearchBar.displayName = fullname
                    break  # Exit loop after the first matching document
                else:
                    # Handle case where no matching document is found
                    print("No user found with that username")
                clickedUname=user_data.get('username')
                Active.activeUser=clickedUname
                print("Login Successfully  ",Active.activeUser)
                return True
            else :
                QMessageBox.critical(self, "Error", "Please Enter valid Username or Password")
                return False

    def open_home_page(self):
        try:
           
            if(self.c2w_submit_form()):
                # Open the Home page
                Active.isLoggedIn=True
                self.home_window = SearchBar()
                self.home_window.showFullScreen()
                self.home_window.show()  # Show the Home page
        except Exception as e:
            print(e)

    def c2w_open_sign_up_page(self, url):
        self.sign_up_window = SignUp()
        self.sign_up_window.showFullScreen()  # Set geometry to match login window
        self.sign_up_window.show()
    

    

    def login_with_google(self):
        pass
        '''
        google_session = OAuth2Session(client_id=google_client_id, scope=google_scope, redirect_uri=google_redirect_uri)
        authorization_url, state = google_session.authorization_url(google_auth_url)

        # Open authorization_url in the default web browser
        import webbrowser
        webbrowser.open(authorization_url)

        # Retrieve authorization response from redirect URI
        redirect_response = input("Enter the full callback URL: ")

        # Fetch access token using the authorization response
        google_session.fetch_token(token_url=google_token_url, authorization_response=redirect_response,
                                   client_secret=google_client_secret)

        # Now you have the access token, you can make requests to Google API using google_session
        # For example:
        # profile_info = google_session.get('https://www.googleapis.com/oauth2/v1/userinfo').json()
        # print("Logged in with Google:", profile_info)
        '''

    def login_with_facebook(self):
        pass
        '''
        facebook_session = OAuth2Session(client_id=facebook_client_id, scope=facebook_scope, redirect_uri=facebook_redirect_uri)
        authorization_url, state = facebook_session.authorization_url(facebook_auth_url)

        # Open authorization_url in the default web browser
        import webbrowser
        webbrowser.open(authorization_url)

        # Retrieve authorization response from redirect URI
        redirect_response = input("Enter the full callback URL: ")

        # Fetch access token using the authorization response
        facebook_session.fetch_token(token_url=facebook_token_url, authorization_response=redirect_response,
                                     client_secret=facebook_client_secret)

        # Now you have the access token, you can make requests to Facebook API using facebook_session
        # For example:
        # profile_info = facebook_session.get('https://graph.facebook.com/me').json()
        # print("Logged in with Facebook:", profile_info)
        '''

'''
if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = LoginPage()
    ex.setWindowIcon(QtGui.QIcon("clickandcollab/assets/login.jpg"))
    ex.setWindowTitle('User Info')
    ex.setGeometry(5, 25, 1350, 700)
    ex.show()
    sys.exit(app.exec_())
'''
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    home = HomePage()
    home.showFullScreen()
    home.show()
    sys.exit(app.exec_())

